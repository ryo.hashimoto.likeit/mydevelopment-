# 使い方
- tomcat起動・停止
```
brew services start tomcat
brew services stop tomcat
```

# 必要事項
- tomcatインストール
```sh
brew services start tomcat
```

# 情報
- tomcatインストールディレクトリ
```sh
/usr/local/Cellar/tomcat/9.0.21
```
- https://gitlab.com/ryo.hashimoto.likeit/mydevelopment-
