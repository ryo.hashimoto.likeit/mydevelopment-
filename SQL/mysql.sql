CREATE DATABASE mydevelopment DEFAULT CHARACTER SET utf8;

USE mydevelopment;

CREATE TABLE user(
  id SERIAL primary key,
  user_property int DEFAULT "1",
  user_name varchar(256) NOT NULL,
  sex varchar(256) NOT NULL,
  login_id  varchar(256) UNIQUE NOT NULL,
  login_password varchar(256) NOT NULL,
  birth_date date,
  create_date date
);

CREATE TABLE favorite(
  id SERIAL primary key,
  user_id int NOT NULL,
  artist_id int NOT NULL
);

CREATE TABLE review(
  id SERIAL primary key,
  user_id int NOT NULL,
  artist_id int NOT NULL,
  performance_rate int NOT NULL,
  technique_rate int NOT NULL,
  character_rate int NOT NULL,
  song_rate int NOT NULL,
  total_rate DOUBLE,
  review_detail TEXT
);

CREATE TABLE artist(
  id SERIAL primary key,
  artist_name varchar(256) NOT NULL,
  user_property int DEFAULT "2",
  region varchar(256),
  introduce TEXT,
  start_date varchar(256),
  profile_pic varchar(256) DEFAULT "sample.jpg",
  login_id varchar(256) UNIQUE NOT NULL,
  password varchar(256),
  create_date date NOT NULL
);

CREATE TABLE member(
  id SERIAL primary key,
  artist_id int NOT NULL,
  member_name varchar(256) NOT NULL
);

CREATE TABLE genre(
  id SERIAL primary key,
  artist_id int NOT NULL,
  genre varchar(256) NOT NULL
);

CREATE TABLE follow(
  id SERIAL primary key,
  artist_id_a int NOT NULL,
  artist_id_p int NOT NULL
);

CREATE TABLE genre_m(
	id int UNIQUE NOT NULL,
	genre varchar(256) NOT NULL
);

INSERT INTO genre_m values
(1,'ROCK'),
(2,'METAL'),
(3,'JAZZ'),
(4,'POPS');

INSERT INTO genre_m values
(5,'CLASSIC'),
(6,'Alternative Rock'),
(7,'shoegazer'),
(8,'post ROCK'),
(9,'HIP HOP'),
(10,'PUNK');


INSERT INTO genre_m values
(1,'Rock'),
(2,'Hard Rock'),
(3,'Heavy Metal'),
(4,'Thrash Metal')
(5,'Death Metal'),
(6,'Metalcore'),
(7,'Progressive Metal'),
(8,'Jazz'),
(9,'Blues'),
(10,'PUNK'),
(11,'Post Rock'),
(12,'Shoegazer'),
(13,'Electro,Techno'),
(14,'Pops')
(15,'Classic'),
(16,'ヴィジュアル系'),
(17,'民族系'),
(18,'ワールドミュージック'),
(19,'Psychedelic rock'),
(20,'Progressive Rock'),
(21,'R&B');
(22,'Fusion');




