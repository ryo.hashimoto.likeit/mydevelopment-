-- TRUNCATE TABLE artist; でartsitテーブルを初期化したあとの想定

TRUNCATE TABLE artist;
TRUNCATE TABLE genre_m;
TRUNCATE TABLE genre;

INSERT INTO genre_m values
(1,'Rock'),
(2,'Hard Rock'),
(3,'Heavy Metal'),
(4,'Thrash Metal'),
(5,'Death Metal'),
(6,'Metalcore'),
(7,'Progressive Metal'),
(8,'Jazz'),
(9,'Blues'),
(10,'PUNK'),
(11,'Post Rock'),
(12,'Shoegazer'),
(13,'Electro,Techno'),
(14,'Pops'),
(15,'Classic'),
(16,'ヴィジュアル系'),
(17,'民族系'),
(18,'ワールドミュージック'),
(19,'Psychedelic rock'),
(20,'Progressive Rock'),
(21,'R&B'),
(22,'Fusion');


INSERT INTO artist (artist_name, region, start_date,login_id,password,create_date) VALUES
('Pink Floyd','イギリス','1965','pinkfloyd','1A1DC91C907325C69271DDF0C944BC72',now()),
('筋肉少女帯','日本・東京','1982','kinsyo','1A1DC91C907325C69271DDF0C944BC72',now()),
('東京事変','日本','2003','jihen','1A1DC91C907325C69271DDF0C944BC72',now()),
('toe','日本','2000','toe','1A1DC91C907325C69271DDF0C944BC72',now()),
('LUNA SEA','日本・神奈川県秦野市','1989','lunasea','1A1DC91C907325C69271DDF0C944BC72',now()),
('Casiopea','日本','1977','casiopea','1A1DC91C907325C69271DDF0C944BC72',now()),
('Johnny Winter','アメリカ合衆国 テキサス州ボーモント','1959','winter','1A1DC91C907325C69271DDF0C944BC72',now()),
('Acid Mothers Temple','日本','1995','amt','1A1DC91C907325C69271DDF0C944BC72',now()),
('Red Hot Chili Peppers','アメリカ合衆国 カリフォルニア州ロサンゼルス','1983','rhcp','1A1DC91C907325C69271DDF0C944BC72',now()),
('Slipknot','アメリカ合衆国アイオワ州デモイン','1995','slipknot','1A1DC91C907325C69271DDF0C944BC72',now()),
('はっぴいえんど','日本・東京都千代田区','1969','happyend','1A1DC91C907325C69271DDF0C944BC72',now()),
('Green Day','アメリカ合衆国カリフォルニア州 バークレー','1987','greenday','1A1DC91C907325C69271DDF0C944BC72',now()),
('ZABADAK','日本','1985','zabadak','1A1DC91C907325C69271DDF0C944BC72',now()),
('coldrain','日本・愛知県名古屋市','2007','coldrain','1A1DC91C907325C69271DDF0C944BC72',now()),
('イエロー・マジック・オーケストラ','日本','1978','ymo','1A1DC91C907325C69271DDF0C944BC72',now()),
('Gargoyle','日本 大阪府','1987','gargoyle','1A1DC91C907325C69271DDF0C944BC72',now()),
('The Album Leaf','アメリカ合衆国カリフォルニア州サン・ディエゴ','1998','albumleaf','1A1DC91C907325C69271DDF0C944BC72',now()),
('上原ひろみ','日本 静岡県','1986','hiromi','1A1DC91C907325C69271DDF0C944BC72',now())
;


INSERT INTO genre (artist_id,genre_id) VALUES
(1,7),(1,19),(1,9),
(2,1),(2,2),(2,3),(2,7),
(3,1),(3,8),(3,14),
(4,11),(4,12),
(5,16),(5,1),
(6,8),(6,22),
(7,9),(7,1),
(8,19),
(9,1),
(10,5),(10,3),(10,6),
(11,1),(11,14),
(12,1),(12,10),
(13,20),(13,17),(13,18),
(14,6),
(15,1),(15,13),
(16,4),(16,3),(16,16),
(17,11),
(18,8),(18,22),
(19,2),(19,9);