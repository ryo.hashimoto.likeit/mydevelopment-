<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>

<c:choose>
	<c:when test="${Artist.totalAvg == 0}">
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>

	<c:when test="${Artist.totalAvg > 0 && Artist.totalAvg < 1}">
		<i class="fas fa-star-half-alt"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>

	<c:when test="${Artist.totalAvg == 1}">
		<i class="fas fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>

	<c:when test="${Artist.totalAvg > 1 && Artist.totalAvg < 2}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star-half-alt"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>
	<c:when test="${Artist.totalAvg == 2}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>

	<c:when test="${Artist.totalAvg > 2 && Artist.totalAvg < 3}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star-half-alt"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>
	<c:when test="${Artist.totalAvg == 3}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="far fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>


	<c:when test="${Artist.totalAvg > 3 && Artist.totalAvg < 4}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star-half-alt"></i>
		<i class="far fa-star"></i>
	</c:when>

	<c:when test="${Artist.totalAvg == 4}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="far fa-star"></i>
	</c:when>

	<c:when test="${Artist.totalAvg > 4 && Artist.totalAvg < 5}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star-half-alt"></i>
	</c:when>

	<c:when test="${Artist.totalAvg == 5}">
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
		<i class="fas fa-star"></i>
	</c:when>
</c:choose>
