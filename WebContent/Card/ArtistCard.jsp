<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


	<c:forEach var="Artist" items="${ArtistList}">
    <div class="artistCard">
      <div class="artistCardHeading">
        <div class="artistCardName"><a href="ArtistPage?id=${Artist.id}">${Artist.name}</a></div>

      </div>
      <div class="artistCardBody">
        <div class="PicFlame">
        	 <img src="${Artist.pic}" style="width:78px; height:78px; border-radius: 5px;">
        	 <c:if test="${property == 1}">
				<div class="fav">

						<c:if test="${Artist.userfavoriteFlag == 1}">
						<!-- 一般ユーザがこのアーティストをお気に入りしている場合 -->
						<div class="remove-fav">

							<form name="addFavForm" method="post" action="ArtistPage">
							<input type="hidden" name="artistId" value="${Artist.id}">
							<input type="hidden" name="favoriteFlag" value="1">
							<input type="hidden" name="property" value="common">
							<button type="submit" class="fav">お気に入り<i class="fas fa-heart"></i></button>
							</form>
						</div>
						</c:if>
						<c:if test="${Artist.userfavoriteFlag == 0}">
						<!-- 一般ユーザがこのアーティストをお気に入りしていない場合 -->
						<div class="add-fav">
							<form name="addFavForm" method="post" action="ArtistPage">
							<input type="hidden" name="artistId" value="${Artist.id}">
							<input type="hidden" name="favoriteFlag" value="2">
							<input type="hidden" name="property" value="common">
							<button type="submit" class="fav">お気に入り<i class="far fa-heart empty-heart"></i></button>
							</form>
						</div>

						</c:if>



				</div>
				</c:if>

			</div>

        <div class="artistCardInfo" style="font-size:12px;">
          <ul>
            <li>
            <c:forEach var="genre" items="${Artist.genre}">
            <a href="#"><span class="mr-1" style="border:solid thin; padding:5px;">${genre}</span></a>
            </c:forEach>
            </li>

            <li class="mt-2">活動地域：
            <span>${Artist.region}</span>
            </li>

            <li>レビュー：
            <span>${Artist.countReview}</span>件（総合平均：<span>${Artist.totalAvg}</span>)
            <div class="mt-1">
				<c:choose>
					<c:when test="${Artist.totalAvg == 0}">
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>

					<c:when test="${Artist.totalAvg > 0 && Artist.totalAvg < 1}">
						<i class="fas fa-star-half-alt"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>

					<c:when test="${Artist.totalAvg == 1}">
						<i class="fas fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>

					<c:when test="${Artist.totalAvg > 1 && Artist.totalAvg < 2}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star-half-alt"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>
					<c:when test="${Artist.totalAvg == 2}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>

					<c:when test="${Artist.totalAvg > 2 && Artist.totalAvg < 3}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star-half-alt"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>
					<c:when test="${Artist.totalAvg == 3}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="far fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>


					<c:when test="${Artist.totalAvg > 3 && Artist.totalAvg < 4}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star-half-alt"></i>
						<i class="far fa-star"></i>
					</c:when>

					<c:when test="${Artist.totalAvg == 4}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="far fa-star"></i>
					</c:when>

					<c:when test="${Artist.totalAvg > 4 && Artist.totalAvg < 5}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star-half-alt"></i>
					</c:when>

					<c:when test="${Artist.totalAvg == 5}">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
					</c:when>
				</c:choose>
			</div>

            </li>
          </ul>
        </div>
      </div>
    </div>



	</c:forEach>

		<script>
		const emptyHeart = document.getElementsByClassName("empty-heart");
		const addFav = document.getElementsByClassName("add-fav");

				for(let i = 0; i < addFav.length; i++){
					addFav[i].onmouseover = function() {
						emptyHeart[i].classList.remove('far');
						emptyHeart[i].classList.add('fas');

				    }
					addFav[i].onmouseout = function() {
						emptyHeart[i].classList.remove('fas');
						emptyHeart[i].classList.add('far');
				    }
				}












	</script>