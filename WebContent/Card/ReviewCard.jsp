<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <c:forEach var="review" items="${reviewList}">
          <li>
            <div class="reviewCard">
              <div class="revierName">
              <span>投稿者名：<span style="font-weight:bold;">${review.userName}</span></span>

				<span class="ml-2">
					<c:choose>
						<c:when test="${review.totalRate == 0}">
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>

						<c:when test="${review.totalRate > 0 && review.totalRate < 1}">
							<i class="fas fa-star-half-alt"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>

						<c:when test="${review.totalRate == 1}">
							<i class="fas fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>

						<c:when test="${review.totalRate > 1 && review.totalRate < 2}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star-half-alt"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>
						<c:when test="${review.totalRate == 2}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>

						<c:when test="${review.totalRate > 2 && review.totalRate < 3}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star-half-alt"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>
						<c:when test="${review.totalRate == 3}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="far fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>


						<c:when test="${review.totalRate > 3 && review.totalRate < 4}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star-half-alt"></i>
							<i class="far fa-star"></i>
						</c:when>

						<c:when test="${review.totalRate == 4}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="far fa-star"></i>
						</c:when>

						<c:when test="${review.totalRate > 4 && review.totalRate < 5}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star-half-alt"></i>
						</c:when>

						<c:when test="${review.totalRate == 5}">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</c:when>
					</c:choose>
				</span>
                <span style="font-size:8px; text-line:right; margin-left:10px;">投稿日時：${review.dateTime}</span>
              </div>

              <div class="reviewContent">
                <div class="reviewRate">
                  <table>
                    <tr>
                      <th>パフォーマンス：</th>
                      <td>${review.performance}</td>
                    <tr>
                      <th>テクニック：</th>
                      <td>${review.technique}</td>
                    <tr>
                      <th>キャラクター：</th>
                      <td>${review.character}</td>
                    <tr>
                      <th>楽曲：</th>
                      <td>${review.song}</td>
                    <tr>
                      <th>総合：</th>
                      <td>${review.totalRate}</td>
                  </table>


                </div>

                <div class="reviewDetail">
                <pre>${review.detail}</pre>

                </div>
              </div>

            </div>
          </li>
		</c:forEach>