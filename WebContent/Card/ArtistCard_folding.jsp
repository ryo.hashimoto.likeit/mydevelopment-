<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<head>

<style>
  .folding {
      height: 70px;
    }

   .hidden{
	  display:none;
   }

   .open{
	  opacity: 0.5;
	  cursor : pointer;
   }

   .close1{
   	  opacity: 0.5;
	  cursor : pointer;
   }

</style>
</head>


	<c:forEach var="Artist" items="${ArtistList}">
    <div class="artistCard folding" id="artistCard">
      <div class="artistCardHeading">
        <div class="artistCardName"><a href="ArtistPage?id=${Artist.id}">${Artist.name}</a></div>

      </div>
      <div class="artistCardBody hidden" id="artistCardBody">

      <div style="font-size:12px;  margin-left:5px;" class="close1">
      	     <i class="fas fa-angle-right fa-lg" ></i>close
      </div>

        <div class="PicFlame">
        	 <img src="${Artist.pic}" style="width:78px; height:78px; border-radius: 5px;">
        </div>
        <div class="artistCardInfo" style="font-size:12px;">
          <ul>
            <li>
            <c:forEach var="genre" items="${Artist.genre}">
            <span class="mr-1" style="border:solid thin; padding:5px;">${genre}</span>
            </c:forEach>
            </li>

            <li class="mt-2">活動地域：
            <span>${Artist.region}</span>
            </li>

            <li>レビュー：
            <span>${Artist.countReview}</span>件（総合平均：<span>${Artist.totalAvg}）</span>
            <div class="mt-1">
            <c:choose>
				<c:when test="${Artist.totalAvg == 0}">
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${Artist.totalAvg > 0 && Artist.totalAvg < 1}">
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${Artist.totalAvg == 1}">
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${Artist.totalAvg > 1 && Artist.totalAvg < 2}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>
				<c:when test="${Artist.totalAvg == 2}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${Artist.totalAvg > 2 && Artist.totalAvg < 3}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>
				<c:when test="${Artist.totalAvg == 3}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>


				<c:when test="${Artist.totalAvg > 3 && Artist.totalAvg < 4}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${Artist.totalAvg == 4}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${Artist.totalAvg > 4 && Artist.totalAvg < 5}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
				</c:when>

				<c:when test="${Artist.totalAvg == 5}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
				</c:when>
			</c:choose>
			</div>
            </li>
          </ul>
        </div>
      </div>
      <div style="font-size:12px; margin-left:5px;" class="open">

      	     <i class="fas fa-angle-right fa-lg" ></i>open
      </div>

    </div>


	</c:forEach>


	<script>
		const card = document.getElementsByClassName("artistCard");
		const cardBody = document.getElementsByClassName("artistCardBody");
		const open = document.getElementsByClassName("open");
		const close = document.getElementsByClassName("close1");

		for(let i = 0; i < open.length; i++){

			open[i].addEventListener('click', () => {
				close[i].classList.remove('hidden');
			    open[i].classList.add('hidden');
			    card[i].classList.remove('folding');
			    cardBody[i].classList.remove('hidden');

			    var cardStyle = card[i].style;

			    cardStyle.height = '200px';
			  });

			open[i].onmouseover = function() {
		        var style = this.style;
		        style.opacity = '1';
		    }
			open[i].onmouseout = function() {
		        var style = this.style;
		        style.opacity = '0.5';
		    }

		}


		for(let i = 0; i < close.length; i++){

			close[i].addEventListener('click', () => {

				close[i].classList.add('hidden');
				open[i].classList.remove('hidden');

			    cardBody[i].classList.add('hidden');
			    var cardStyle = card[i].style;

			    cardStyle.height = '70px';

			  });

			close[i].onmouseover = function() {
		        var style = this.style;
		        style.opacity = '1';
		    }
			close[i].onmouseout = function() {
		        var style = this.style;
		        style.opacity = '0.5';
		    }

		}
	</script>



