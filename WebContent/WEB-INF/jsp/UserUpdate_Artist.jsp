<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>


<jsp:include page="/baselayout/head.html" />

<title>トップページ - 情報更新</title>
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />


<div class="main">
  <div class="container_l">

	<jsp:include page="/baselayout/ArtistPageSide.jsp" />

    <div class="myPageContent">
      <h4>アーティスト情報更新</h4>
      <hr>


      <div class="artistRegistForm">
        <form action="UserUpdateConfirm_Artist">
          <table>
            <tr>
              <th>アーティスト名</th>
              <td><input type="text" name="artistName" value="アーティスト名"></td>
            <tr>
              <th>プロフィール画像</th>
              <td><input type="file" accept=".jpg,.gif,.png,image/gif,image/jpeg,image/png"></td>
            <tr>
              <th>活動開始日</th>
              <td><input type="date" name="birthDate"></td>
            <tr>
              <th>ジャンル</th>
              <td>
                <input type="checkbox" name="genre" value="1">Rock
                <input type="checkbox" name="genre" value="2">Pops
                <input type="checkbox" name="genre" value="3">Jazz
                <input type="checkbox" name="genre" value="4">Metal
              </td>
            <tr>
              <th>メンバー</th>
              <td>
              <span class="member">
                <input type="text" value="メンバー1"><br>
                <input type="text" value="メンバー2"><br>
                <input type="text" value="メンバー3"><br>
                <input type="text"><br>
                <input type="text"><br>
              </span>
              </td>
            <tr>
              <th>紹介文</th>
              <td>
                <textarea name="introduce" cols="50" rows="10"></textarea>
              </td>
            <tr>
              <th>ログインID</th>
              <td><input type="text" name="loginId" value="ログインID"></td>
            <tr>
              <th>パスワード</th>
              <td><input type="password" name="passwod"></td>
            <tr>
              <th>パスワード（確認）</th>
              <td><input type="password" name="password2"></td>

          </table>
          <div class="text-center mt-5">
            <input class="btn btn-dark" type="submit" value="更新">
          </div>
        </form>

        <div class="text-right">
          <a href="UserDelete_Artist" class="btn btn-danger active" role="button" aria-pressed="true">削除</a>
        </div>
      </div>

    </div>


  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
