<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>

<jsp:include page="/baselayout/head.html" />

<title>更新確認</title>
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />



<div class="main">
  <div class="container_l">

 	<jsp:include page="/baselayout/ArtistPageSide.jsp" />

    <div class="myPageContent">
      <h4>更新確認</h4>
      <hr>


      <div class="artistRegistForm">
        <form>
          <table>
            <tr>
              <th>アーティスト名</th>
              <td>Nuvelle Vague</td>
            <tr>
              <th>プロフィール画像</th>
              <td>
                <div class="artistPic1">
                  <img src="sample.jpg">
                </div>
              </td>
            <tr>
              <th>活動開始日</th>
              <td>2019年6月13日</td>
            <tr>
              <th>ジャンル</th>
              <td>
                ROCK,METAL
              </td>
            <tr>
              <th>メンバー</th>
              <td>
              <span class="member">
                <ul>
                  <li>橋本</li>
                  <li>秋山</li>
                </ul>
              </span>
              </td>
            <tr>
              <th>紹介文</th>
              <td>
                紹介文紹介部紹介文紹介部紹介文紹介部紹介文紹介部紹介文紹介部紹介文紹介部紹介文紹介部紹介文紹介部紹介文紹介部
              </td>
            <tr>
              <th>ログインID</th>
              <td>loginID</td>


          </table>
          <div class="text-center mt-5">
            <input class="btn btn-dark" type="submit" value="更新">
          </div>
        </form>
      </div>

    </div>


  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
