<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>

<jsp:include page="/baselayout/head.html" />

<title>検索結果</title>
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />


<div class="main">

<div class="container_l">

<jsp:include page="/baselayout/SideSearch.jsp" />

<div class="content_l">
  <div class="showing">
		<c:choose>
			<c:when test="${pageNum == 1}">
			<%-- 1ページ目のとき --%>
				<c:choose>
				<%-- 総ページ数が1ページだけの場合 --%>

				<c:when test="${TotalPage == 1}">
				検索結果： ${count}件中 ${(pageNum - 1) * maxItem + 1}～${count}件を表示
				<br>
				${pageNum}ページ目
				</c:when>

				<%-- そうではない場合 --%>
				<c:otherwise>
				検索結果： ${count}件中 ${(pageNum - 1) * maxItem + 1}～${maxItem * pageNum}件を表示
				<br>
				${pageNum}ページ目
				</c:otherwise>
				</c:choose>
			</c:when>

			<c:when test="${pageNum == TotalPage}">
			<%-- 最終ページのとき --%>
				検索結果：${count}件中 ${(pageNum - 1) * maxItem + 1}～${count}件を表示
				<br>
				${pageNum}ページ目
			</c:when>

			<c:otherwise>
			<%-- それ以外 --%>
				検索結果：${count}件中 ${(pageNum - 1) * maxItem + 1}～${maxItem * pageNum}件を表示
				<br>
				${pageNum}ページ目
			</c:otherwise>

		</c:choose>

  </div>

  <div class="ResultArrangement">
  <c:choose>
  	<c:when test="${arrange == 0}">
	  	 <ul>
	  		<li><input type="radio" name="arrange" id="normal" checked>通常</li>
	  		<li><input type="radio" name="arrange" id="arrangeRate">レートの高い順</li>
	  	</ul>
  	</c:when>
  	<c:when test="${arrange == 1}">
	  	 <ul>
	  		<li><input type="radio" name="arrange" id="normal" >通常</li>
	  		<li><input type="radio" name="arrange" id="arrangeRate" checked>レートの高い順</li>
	  	</ul>
  	</c:when>

  </c:choose>


    <script type="text/javascript">
    	var normal = document.getElementById('normal');
    	normal.addEventListener('click', function(){

    	document.normal.submit();
    	})

    	var normal = document.getElementById('arrangeRate');
    	normal.addEventListener('click', function(){
    	document.rate.submit();
    	})

    </script>

  </div>

	<jsp:include page="/Card/ArtistCard.jsp" />

	<div class="pagingContainer">
	<div class="paging">
	<c:forEach var="i" begin="1" end="${TotalPage}" step="1">
			<form action="SearchResult" method="post">
				<input type="hidden" value="${i}" name="pageNum">
				<input type="hidden" value="${name}" name="name">
				<input type="hidden" value="${region}" name="region">

				<c:choose>
					<c:when test="${arrange == 0}">
						<input type="hidden" value="normal" name="arrange">
					</c:when>
					<c:when test="${arrange == 1}">
						<input type="hidden" value="rate" name="arrange">
					</c:when>
				</c:choose>


				<c:forEach var="genre" items="${genreId}">
					<input type="hidden" name="genre" value="${genre}" >
				</c:forEach>


				<c:choose>
					<c:when test="${pageNum == i}">
						<span style="margin-right:10px;">${i}</span>
					</c:when>
					<c:otherwise>
						<a style="margin-right:10px;" href="javascript:void(0)" onclick="this.parentNode.submit()">${i}</a>
					</c:otherwise>
				</c:choose>

			</form>

	</c:forEach>




	</div>





	</div>


  </div>

</div>

</div>


<footer></footer>

<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
