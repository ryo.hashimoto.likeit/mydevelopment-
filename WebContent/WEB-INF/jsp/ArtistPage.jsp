<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<jsp:include page="/baselayout/head.html" />

<title>${artist.name}</title>
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />



<div class="main">

<div class="container_l">

<jsp:include page="/baselayout/SideSearch.jsp" />

<div class="content_l">
  <div class="artistName">
    <h3>${artist.name}</h3>
    <hr>
  </div>
<div class="artistInfo_1">
  <div class="artistInfoBasic">
    <table>
      <tbody>
        <tr id="startDate">
          <th>活動開始日</th>
          <td>${artist.startDate}</td>

        <tr id="genre">
          <th>ジャンル</th>
          <td>
          <c:forEach var="genre" items="${artist.genre}">
          <span class="mr-1">${genre}</span>
          </c:forEach>
          </td>

        <tr id="region">
          <th>活動地域</th>
          <td>${artist.region}</td>

        <tr id="member">
          <th>メンバー</th>
          <td>
            <ul>
               <c:forEach var="member" items="${artist.member_list}">
         		 <li>${member}</li>
          	   </c:forEach>
            </ul>
          </td>
      </tbody>
    </table>

  </div>

  <div class="artistPic1">
    <img src="${artist.pic}">
  </div>

  </div>



<div class="artistInfo_2">
  <div class="introduce">
    <h5>紹介文</h5>
    <hr>
     	<pre>${artist.introduce}</pre>
    <hr>
  </div>
  <div class="reviewContent">
    <h5>レビュー</h5>
    <hr>
      <div class="review_total">
        <span class="lead mr-2">総合評価：<span style="font-weight:bold;">${reviewAvg.totalRate_ave}</span></span>
	        <c:choose>
				<c:when test="${reviewAvg.totalRate_ave == 0}">
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave > 0 && reviewAvg.totalRate_ave < 1}">
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave == 1}">
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave > 1 && reviewAvg.totalRate_ave < 2}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>
				<c:when test="${reviewAvg.totalRate_ave == 2}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave > 2 && reviewAvg.totalRate_ave < 3}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>
				<c:when test="${reviewAvg.totalRate_ave == 3}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>


				<c:when test="${reviewAvg.totalRate_ave > 3 && reviewAvg.totalRate_ave < 4}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave == 4}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="far fa-star"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave > 4 && reviewAvg.totalRate_ave < 5}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star-half-alt"></i>
				</c:when>

				<c:when test="${reviewAvg.totalRate_ave == 5}">
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
				</c:when>
			</c:choose>
        <span class="mr-2 ml-2">パフォーマンス：${reviewAvg.performance_ave}</span>
        <span class="mr-2">テクニック：${reviewAvg.technique_ave}</span>
        <span class="mr-2">キャラクター：${reviewAvg.character_ave}</span>
        <span class="mr-2">楽曲：${reviewAvg.song_ave}</span>

      </div>
      <div class="reviewList">
        <ul>
			<jsp:include page="/Card/ReviewCard.jsp" />
        </ul>
      </div>
    <hr>
  </div>
</div>

	<c:choose>
		<c:when test="${SessionUser.property == null}">
			<%-- ●ゲストユーザの場合 --%>
			<p>お気に入り、レビュー機能を利用するには、新規登録orログインをしてください</p>
		<a href="NewRegist" class="btn btn-light btn-lg active" role="button" aria-pressed="true">新規登録</a>
		<a href="Login" class="btn btn-light btn-lg active" role="button" aria-pressed="true">ログイン</a>
		</c:when>

		<c:when test="${SessionUser.property == 1}">
			<%-- ●一般ユーザの場合 --%>
			<c:choose>
				<c:when test="${favoriteFlag == 1}">
				<%-- 既にお気に入りしている場合 --%>
					<form action="#" method="post" style="display:inline-block;">
						<input class="btn btn-dark btn-lg" type="submit" value="お気に入りから外す">
						<input type="hidden" name="artistId" value="${artist.id}">
						<input type="hidden" name="favoriteFlag" value="1">
						<input type="hidden" name="property" value="common">
					</form>
				</c:when>
				<c:when test="${favoriteFlag != 1}">
				<%-- お気に入りしていない場合 --%>
					<form action="ArtistPage" method="post" style="display:inline-block;">
						<input class="btn btn-light btn-lg" type="submit" value="お気に入りに追加">
						<input type="hidden" name="artistId" value="${artist.id}">
						<input type="hidden" name="favoriteFlag" value="2">
						<input type="hidden" name="property" value="common">
					</form>
				</c:when>
			</c:choose>


		<a href="Review?id=${artist.id}" class="btn btn-light btn-lg active" role="button" aria-pressed="true">レビューする</a>

		</c:when>

		<c:when test="${SessionUser.property == 2}">
			<%-- ●アーティストユーザの場合 --%>
						<c:choose>
				<c:when test="${favoriteFlag == 1}">
				<%-- 既にフォローしている場合 --%>
					<form action="ArtistPage" method="post" style="display:inline-block;">
						<input class="btn btn-dark btn-lg" type="submit" value="フォロー解除">
						<input type="hidden" name="artistId" value="${artist.id}">
						<input type="hidden" name="favoriteFlag" value="1">
						<input type="hidden" name="property" value="artist">
					</form>
				</c:when>
				<c:when test="${favoriteFlag != 1}">
				<%-- フォローしていない場合 --%>
					<form action="ArtistPage" method="post" style="display:inline-block;">
						<input class="btn btn-light btn-lg" type="submit" value="フォローする">
						<input type="hidden" name="artistId" value="${artist.id}">
						<input type="hidden" name="favoriteFlag" value="2">
						<input type="hidden" name="property" value="artist">
					</form>
				</c:when>
			</c:choose>



		</c:when>
	</c:choose>



</div>

</div>

</div>


<footer></footer>

<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
