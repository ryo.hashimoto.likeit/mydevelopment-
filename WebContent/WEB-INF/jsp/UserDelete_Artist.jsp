<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>

<jsp:include page="/baselayout/head.html" />

<title>削除確認</title>
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />



<div class="main">
  <div class="container_l">
  	<jsp:include page="/baselayout/ArtistPageSide.jsp" />

    <div class="myPageContent">
      <h4>ユーザ情報削除</h4>
      <hr>


      <div class="userUpdateForm">
        <h5 class="text-center my-3">本当に削除しますか。</h5>
          <div class="text-center my-5">
            <a href="UserUpdate_Artist" class="btn btn-dark active mr-5" role="button" aria-pressed="true">戻る</a>
            <a href="Index" class="btn btn-danger active" role="button" aria-pressed="true">削除</a>
          </div>
      </div>

    </div>


  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
