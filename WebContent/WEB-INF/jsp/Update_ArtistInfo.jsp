<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@page import="model.ArtistUser"%>
    <%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<jsp:include page="/baselayout/head.html" />

<title>アーティストページ</title>
</head>

<body>


<jsp:include page="/baselayout/header.jsp" />




<div class="main">
  <div class="container_l">
	<jsp:include page="/baselayout/ArtistPageSide.jsp" />

    <div class="myPageContent">
		<form action="Update_ArtistInfo" method="post">
        <h3><input type="text" name="name" value="${ArtistInfo.name}"></h3>
        <hr>
    <div class="artistInfo_1">
      <div class="artistInfoBasic">
        <table>
          <tbody>
            <tr id="startDate">
              <th>活動開始日</th>
              <td>
             	<input type="date" name="startDate" value="${ArtistInfo.startDate}">
              </td>

            <tr id="genre">
              <th>ジャンル</th>
              <td>
			<c:forEach var="genreM" items="${genre_master}" >
				<input type="checkbox" name="genre" value="${genreM.id}" id="${genreM.id}">${genreM.name}

				<c:forEach var="genre" items="${ArtistInfo.genreId}">
				<script type="text/javascript">
				var genre = document.getElementById('${genreM.id}');

				var flag = 0;

				if(${genreM.id} == ${genre}){
					flag += 1;
				}

				if(flag == 1){
					genre.checked = true;
				}

				</script>
				</c:forEach>

			</c:forEach>

              </td>

            <tr id="region">
              <th>活動地域</th>
              <td>
              	<input type="text" name="region" value="${ArtistInfo.region}">
              </td>

            <tr id="member">
              <th>メンバー</th>
              <td>
                <ul id="memberlist">
                <%
                ArtistUser ArtistInfo = (ArtistUser) request.getAttribute("ArtistInfo");
                ArrayList<String>  member_list = ArtistInfo.getMember_list();
                int member_count = 0;

                if(member_list.size() <= 5){
               	 member_count = 5 - member_list.size();
                }

                %>
              	<c:forEach var="member" items="${ArtistInfo.member_list}" >
					<input type="text" name="member" value="${member}">
					<br>
				</c:forEach>

				<c:forEach var="i" begin="1" end="<%=member_count %>">
					<li><input type="text" name="member" value=""></li>
				</c:forEach>



                </ul>
                <a href="javascript: void(0);" onclick="plus();" id="addmember">＋入力枠を追加する</a>

                <script>
                	var plus = function(){

                		var input_li = document.createElement('li');

                		var input_node = document.createElement('input');
                		input_node.setAttribute('type', 'text');
                		input_node.setAttribute('name', 'member');

                		input_li.appendChild(input_node);


                		document.getElementById('memberlist').appendChild(input_li);
                	}

                </script>
              </td>

          </tbody>
        </table>

      </div>

      <div class="artistPic1" style="text-align:center;">
        <img src="${ArtistInfo.pic}">
        <input class="mt-2" type="file" name="file" form="pic-form" accept=".jpg">
        <p style="color:red; font-size:12px;">★jpg形式の画像のみ登録可能</p>
		<input class="btn btn-dark mt-2" type="submit" value="アップロード" form="pic-form">


      </div>

      </div>



    <div class="artistInfo_2">
      <div class="introduce">
        <h5>紹介文</h5>
        <hr>
        <textarea name="introduce" cols="80" rows="10">${ArtistInfo.introduce}</textarea>
        <hr>
      </div>

    </div>
		<div class="text-center">
			<input type="hidden" name="id" value="${SessionUser.id}">
			<input class="btn btn-dark" type="submit" value="更新">
		</div>

	</form>

	<form method="POST" enctype="multipart/form-data" action="Pic_upload" id="pic-form">

	</form>

    </div>


  </div>


</div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>