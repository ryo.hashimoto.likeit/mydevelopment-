<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>

<jsp:include page="/baselayout/head.html" />

<title>マイページ</title>
</head>


<body>
<jsp:include page="/baselayout/header.jsp" />

<div class="main">
  <div class="container_l">

	<jsp:include page="/baselayout/MyPageSide.jsp" />

    <div class="myPageContent">
      <h4>更新確認</h4>
      <hr>


      <div class="userUpdateForm">
        <form action="UserUpdateConfirm" method="post">
          <table>
            <tr>
              <th>性別</th>
              <td>${SessionUser.sex}</td>
            <tr>
              <th>生年月日</th>
              <td>${user.birthDate}</td>
            <tr>
              <th>ユーザ名</th>
              <td>${user.name}</td>
            <tr>
              <th>ログインID</th>
              <td>${SessionUser.loginId}</td>

          </table>
          <div class="mt-3 text-center text-danger" >
          	${errorMessage}
          </div>

          <div class="text-center mt-5">
            <input class="btn btn-dark" type="submit" value="更新">
            <input type="hidden" name="id" value="${SessionUser.id}">
            <input type="hidden" name="birthDate" value="${user.birthDate}">
            <input type="hidden" name="name" value="${user.name}">
          </div>
        </form>
      </div>

    </div>


  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>