<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<jsp:include page="/baselayout/head.html" />
</head>

<body>


<jsp:include page="/baselayout/header.jsp" />



<div class="main">
  <div class="container_s">
    <h3>ログイン</h3>
    <hr>
    <div class="content_s">


      <div class="contentForm_s">

          <form action="Login" method="post">
            <table>
              <tbody>
              	<tr>
              	  <th>ユーザタイプ</th>
              	  <td>
              	  	<input type="radio" name="property" value="common" checked>一般
              	  	<input type="radio" name="property" value="artist">アーティスト
              	  </td>
                <tr id="loginId">
                  <th>ログインID</th>
                  <td>
                    <input type="text" name="loginId">
                  </td>
                <tr id="password">
                  <th>パスワード</th>
                  <td>
                    <input type="password" name="password">
                  </td>
              </tbody>
            </table>
            <div class="registButton">
            <input class="btn btn-dark" type="submit" value="ログイン">
            </div>
          </form>

		<div style="text-align:center; margin-top:5px;">
			<span style="color:red; ">${errorMassage2}</span>
		</div>
      </div>

    </div>
  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
