<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>

</head>

<jsp:include page="/baselayout/head.html" />

<title>一般ユーザ新規登録確認</title>
</head>

<body>


<jsp:include page="/baselayout/header.jsp" />



<div class="main">
  <div class="container_s">
    <h3>一般ユーザ新規登録</h3>
    <hr>
    <div class="content_s">
      <div class="contentForm_s">
        <h5>登録確認</h5>
        <hr>
          <form action="NewRegist_Result" method="post">
            <table>
              <tbody>
                <tr id="sex">
                  <th>性別</th>
                  <td>
                    ${user.sex}
                    <input type="hidden" name="sex" value="${user.sex}">
                  </td>

                <tr id="birthDate">
                  <th>生年月日</th>
                  <td>
					${user.birthDate}
					<input type="hidden" name="birthDate" value="${user.birthDate}">
                  </td>

                <tr id="userName">
                  <th>ユーザ名</th>
                  <td>
                    ${user.name}
                    <input type="hidden" name="userName" value="${user.name}">
                  </td>

                <tr id="loginId">
                  <th>ログインID</th>
                  <td>
					${user.loginId}
					<input type="hidden" name="loginId" value="${user.loginId}">
                  </td>


              </tbody>
            </table>

            <div class="registButton">
            <input type="hidden" name="password" value="${user.password}">
            <input class="btn btn-dark" type="submit" value="登録">
            </div>
          </form>

      </div>

    </div>
  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>