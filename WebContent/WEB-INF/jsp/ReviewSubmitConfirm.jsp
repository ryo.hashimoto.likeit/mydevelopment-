<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="ja">
<head>

<jsp:include page="/baselayout/head.html" />

<title>レビュー投稿確認</title>
</head>

<body>

<jsp:include page="/baselayout/header.jsp" />


<div class="main">

<div class="container_l">

<jsp:include page="/baselayout/SideSearch.jsp" />

<div class="content_l">
  <div class="artistName">
    <h3>レビュー投稿確認</h3>
  </div>

  ><a href="ArtistPage?id=${artist.id}">${artist.name}</a>




    <div class="reviewContent">
            <div class="reviewCard">
              <div class="revierName">
              	投稿者名：${user.name}
              </div>
              <div class="reviewContent">
                <div class="reviewRate">
                  <table>
                    <tr>
                      <th>パフォーマンス</th>
                      <td>${review.performance}</td>

                    <tr>
                      <th>テクニック</th>
                      <td>${review.technique}</td>
                    <tr>
                      <th>キャラクター</th>
                      <td>${review.character}</td>
                    <tr>
                      <th>楽曲</th>
                      <td>${review.song}</td>
                    <tr>
                      <th>総合</th>
                      <td>${review.totalRate}</td>
                  </table>
                </div>

                <div class="reviewDetail">
                  <pre>${review.detail}</pre>

                </div>
              </div>
            </div>
      </div>
	<form method="post" action="ReviewSubmitConfirm">
      <div class="text-center">
       <input type="hidden" name="performance" value="${review.performance}">
       <input type="hidden" name="technique" value="${review.technique}">
       <input type="hidden" name="character" value="${review.character}">
       <input type="hidden" name="song" value="${review.song}">
       <input type="hidden" name="totalRate" value="${review.totalRate}">
       <input type="hidden" name="detail" value="${review.detail}">
       <input type="hidden" name="artistId" value="${artist.id}">
       <input type="hidden" name="userId" value="${user.id}">
        <input class="btn btn-dark mt-2 w-50" type="submit" value="投稿">
      </div>

     </form>
${error}
  </div>


</div>

</div>



<footer></footer>

<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
