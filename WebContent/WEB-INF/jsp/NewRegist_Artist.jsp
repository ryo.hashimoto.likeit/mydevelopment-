<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>

<jsp:include page="/baselayout/head.html" />

<title>アーティストユーザ新規登録</title>
</head>

<body>


<jsp:include page="/baselayout/header.jsp" />




<div class="main">
  <div class="container_s">
    <h3>アーティストユーザ新規登録</h3>
    <hr>

    <div class="artistRegistForm">
      <form action="NewRegist_Artist" method="post">
        <table>
          <tr>
            <th>アーティスト名</th>
            <td><input type="text" name="name"></td>
          <tr>
            <th>ログインID</th>
            <td><input type="text" name="loginId"></td>
          <tr>
            <th>パスワード</th>
            <td><input type="password" name="password"></td>
          <tr>
            <th>パスワード（確認）</th>
            <td><input type="password" name="password2"></td>


        </table>
        	<div class="text-center text-danger mt-3">
        		${errorMessage}
        	</div>
        <div class="text-center mt-5">
          <input class="btn btn-dark" type="submit" value="登録">
        </div>
      </form>
    </div>

  </div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>