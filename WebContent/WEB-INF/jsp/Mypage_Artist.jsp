<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<jsp:include page="/baselayout/head.html" />

<title>アーティストページ</title>
</head>

<body>


<jsp:include page="/baselayout/header.jsp" />




<div class="main">
  <div class="container_l">
	<jsp:include page="/baselayout/ArtistPageSide.jsp" />

    <div class="myPageContent">

        <h3><a href="ArtistPage?id=${ArtistInfo.id}">${ArtistInfo.name}</a></h3>
        <hr>
    <div class="artistInfo_1">


      <div class="artistInfoBasic">

        <table>
          <tbody>
            <tr id="startDate">
              <th>活動開始日</th>
              <td>${ArtistInfo.startDate}</td>

            <tr id="genre">
              <th>ジャンル</th>
              <td style="line-height:0.7;">

              		<c:forEach var="genre" items="${ArtistInfo.genre}"  >
					 <p style="margin:0; padding-top:0px; padding-bottom:0px;">${genre}</p>
			　		</c:forEach>

              </td>




            <tr id="region">
              <th>活動地域</th>
              <td>${ArtistInfo.region}</td>

            <tr id="member">
              <th>メンバー</th>
              <td style="line-height:0.7;">
              	<ul>
              		<c:forEach var="member" items="${ArtistInfo.member_list}"  >
						<li>${member}</li>
			　		</c:forEach>
            	</ul>
              </td>

          </tbody>
        </table>

      </div>

      <div class="artistPic1">
        <img src="${ArtistInfo.pic}">
      </div>

      </div>



    <div class="artistInfo_2">
      <div class="introduce">
        <h5>紹介文</h5>
        <hr>
        <pre>${ArtistInfo.introduce}</pre>
        <hr>
      </div>
       <div class="text-right">
        <a href="Update_ArtistInfo" class="btn btn-dark btn-lg active" role="button" aria-pressed="true">プロフィール変更</a>
      </div>



	<div class="artistInfo_3 mt-3">
		<h5>ステータス</h5>
		<hr>
		<table border="1" cellspacing="3" cellpadding="8"  style="font-weight: lighter; ">
			<tr>
			<th>登録日時</th>
			<th>被お気に入り数</th>
			<th>フォロワー数</th>
			<th>被レビュー件数</th>
			<th>総合評価</th>
			</tr>

			<tr style="font-size:12px;" align="center">
			<th>${ArtistInfo.createDate}</th>
			<th>${ArtistInfo.favorite_count}</th>
			<th>${ArtistInfo.follow_count}</th>
			<th><a href="ReviewHistory_Artist">${ArtistInfo.countReview}</a></th>
			<th>${ArtistInfo.totalAvg}</th>

			</tr>

		</table>
		<br>
		<p style="color:red; font-size:10px; margin:0;">被お気に入り数 : 一般ユーザからお気に入りされている数</p>
		<p style="color:red; font-size:10px;">フォロワー数 : アーティストユーザからフォローされている数</p>
	</div>
    </div>


    </div>


  </div>


</div>


</div>

<footer></footer>


<!-- Optional JavaSxcript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
