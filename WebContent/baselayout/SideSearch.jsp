<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="SearchForm">
  <form action="SearchResult" method="post" name="normal">
    <div>
      <div class="mb-4">
      <label>アーティスト名</label>
      <input type="text" name="name" value="${name}">
      </div>

      <div class="mb-4">
      <label>活動地域</label>
      <br>
		<input type="text" name="region" value="${region}">
      </div>


       <div class="mb-4">
      <label>ジャンル</label>
      <br>
		<div  style="text-align:left; margin-left:20px;">
		<c:forEach var="genreM" items="${genre_master}" >

		<ul  style="padding:0; margin:0; font-size: 12px;" >
			<li><input type="checkbox" name="genre" value="${genreM.id}" id="${genreM.id}">${genreM.name}</li>
		</ul>



			<c:if test="${genreId != null}">
			<c:forEach var="genre" items="${genreId}">
			<script type="text/javascript">
			var genre = document.getElementById('${genreM.id}');

			var flag = 0;

			if(${genreM.id} == ${genre}){
				flag += 1;
			}

			if(flag == 1){
				genre.checked = true;
			}

			</script>
			</c:forEach>
			</c:if>

		</c:forEach>

		</div>
      </div>


    </div>

    <input class="btn btn-light" type="submit" value="検索">
  </form>


	<!-- レート高い順用の不可視のフォーム -->
  	<form action="SearchResult" method="post" name="rate">
		<input type="hidden" value="${name}" name="name">
		<input type="hidden" value="${region}" name="region">
		<c:forEach var="genre" items="${genreId}">
			<input type="hidden" name="genre" value="${genre}" >
		</c:forEach>
		<input type="hidden" value="rate" name="arrange">
	</form>

</div>