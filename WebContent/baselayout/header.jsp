<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="Index">Title</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="Index">Home</a>
        </li>

      </ul>
    </div>
  </nav>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">

	<c:choose>
		<c:when test="${SessionUser.property == null}">
			<%-- ゲストユーザの場合 --%>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="NewRegist">新規登録</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="Login">ログイン</a>
			</li>
		</ul>
		</c:when>
		<c:when test="${SessionUser.property == 1}">
			<%-- 一般ユーザの場合 --%>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="MyPage"><i class="far fa-user mr-2 fa-user-c"></i>${SessionUser.name}</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="Logout">ログアウト</a>
			</li>
		</ul>
		</c:when>

		<c:when test="${SessionUser.property == 2}">
			<%-- アーティストユーザの場合 --%>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="MyPage_Artist"><i class="fas fa-user mr-2 fa-user-a"></i>${SessionUser.loginId}</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="Logout">ログアウト</a>
			</li>
		</ul>
		</c:when>
	</c:choose>


    </div>
  </nav>