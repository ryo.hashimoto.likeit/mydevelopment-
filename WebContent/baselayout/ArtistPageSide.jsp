<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

    <div class="myPageSide">
      <h4>${SessionUser.loginId}</h4>
      <ul>
        <li><a href="MyPage_Artist" class="btn btn-secondary btn-sm btn-sm w-75 active" role="button" aria-pressed="true">マイページTOP</a></li>
        <li><a href="Update_ArtistInfo" class="btn btn-secondary btn-sm btn-sm w-75 active" role="button" aria-pressed="true">プロフィール更新</a></li>
        <li><a href="Follow_Artist" class="btn btn-secondary btn-sm btn-sm w-75 active" role="button" aria-pressed="true">フォローリスト</a></li>
        <li><a href="ReviewHistory_Artist" class="btn btn-secondary btn-sm btn-sm w-75 active" role="button" aria-pressed="true">被レビューリスト</a></li>
      </ul>
    </div>