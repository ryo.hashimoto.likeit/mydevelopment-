package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ReviewDAO;
import model.ReviewBeans;

/**
 * Servlet implementation class ReviewDelete
 */
@WebServlet("/ReviewDelete")
public class ReviewDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		int id = Integer.parseInt(request.getParameter("id"));
		ArrayList<ReviewBeans> reviewList = new ArrayList<ReviewBeans>();

		reviewList = ReviewDAO.getReviewByReviewId(id);

		request.setAttribute("reviewList", reviewList);

		request.setAttribute("delete", 1);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewDeleteConfirm.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		int id = Integer.parseInt(request.getParameter("id"));

		int result = ReviewDAO.deleteReview(id);

		if(result == 1) {
			response.sendRedirect("MyPage");
		}else {
			response.sendRedirect("Index");
		}
	}

}
