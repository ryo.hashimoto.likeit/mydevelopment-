package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import dao.ReviewDAO;
import model.ArtistUser;
import model.ReviewBeans;

/**
 * Servlet implementation class ReviewHistory_artist
 */
@WebServlet("/ReviewHistory_Artist")
public class ReviewHistory_Artist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewHistory_Artist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		HttpSession session = request.getSession();
		ArtistUser artist_user = (ArtistUser)session.getAttribute("SessionUser");

		if (artist_user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		int artistId =  artist_user.getId();

		ArtistUser ArtistInfo = ArtistDataDAO.findById(artistId);

		request.setAttribute("ArtistInfo", ArtistInfo);

		ArrayList<ReviewBeans> reviewList = ReviewDAO.getReviewByArtist(artistId);
		request.setAttribute("reviewList", reviewList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewHistory_Artist.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
