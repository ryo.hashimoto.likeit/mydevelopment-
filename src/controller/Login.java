package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import dao.UserDataDAO;
import model.ArtistUser;
import model.CommonUser;
import model.Other;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html; charset=UTF-8");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String property = request.getParameter("property");

		String loginId = request.getParameter("loginId");
		String password = Other.encrypt(request.getParameter("password"));


		if(property.equals("common")) {

			CommonUser user = UserDataDAO.Login(loginId, password);

			if(user == null) {
				request.setAttribute("errorMassage", "ログインに失敗しました");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
				dispatcher.forward(request, response);
				return;
			}

			HttpSession session = request.getSession();
			session.setAttribute("SessionUser", user);

			HttpSession sessionProperty = request.getSession();
			sessionProperty.setAttribute("property", 1);

			response.sendRedirect("MyPage");


		}else if(property.equals("artist")) {

			ArtistUser artist = ArtistDataDAO.Login(loginId,password);

			if(artist == null) {
				request.setAttribute("errorMassage2", "ログインに失敗しました");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
				dispatcher.forward(request, response);
				return;
			}

			HttpSession session = request.getSession();
			session.setAttribute("SessionUser", artist);

			HttpSession sessionProperty = request.getSession();
			sessionProperty.setAttribute("property", 2);

			response.sendRedirect("MyPage_Artist");
		}








	}

}
