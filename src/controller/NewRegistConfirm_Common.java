package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDataDAO;
import model.CommonUser;

/**
 * Servlet implementation class NewRegistConfirm_Common
 */
@WebServlet("/NewRegistConfirm_Common")
public class NewRegistConfirm_Common extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewRegistConfirm_Common() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String sex = request.getParameter("sex");
		String birthDate = request.getParameter("birthDate");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");





		int errorNum = 0;


		if(sex == null || birthDate.isEmpty() || name.isEmpty() || loginId.isEmpty() || password.isEmpty() ||  password2.isEmpty()){
			errorNum = 1;
		}else if(!password.equals(password2)) {
			errorNum = 2;
		}else if(UserDataDAO.SearchloginId(loginId) == 1){
			errorNum = 3;
		}else {
			errorNum = 0;
		}



		switch(errorNum) {
			case 0:
				//成功
				CommonUser user = new CommonUser(sex,birthDate,name,loginId,password);
				request.setAttribute("user", user);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewRegistConfirm_Common.jsp");
				dispatcher.forward(request, response);
				break;

			case 1:
				request.setAttribute("errorMessage", "未入力の項目があります");

				RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Common.jsp");
				dispatcher2.forward(request, response);
				break;

			case 2:
				request.setAttribute("errorMessage", "パスワードが異なっています");

				RequestDispatcher dispatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Common.jsp");
				dispatcher3.forward(request, response);
				break;

			case 3:
				request.setAttribute("errorMessage", "既にそのログインIDは存在します");

				RequestDispatcher dispatcher4 = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Common.jsp");
				dispatcher4.forward(request, response);
				break;


		}






	}

}
