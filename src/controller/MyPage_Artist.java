package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import model.ArtistUser;

/**
 * Servlet implementation class MyPage_Artist
 */
@WebServlet("/MyPage_Artist")
public class MyPage_Artist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyPage_Artist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html; charset=UTF-8");
		 HttpSession session = request.getSession();
		 ArtistUser user = (ArtistUser)session.getAttribute("SessionUser");


		if (user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		 int sessionId = user.getId();

		 ArtistUser ArtistInfo = ArtistDataDAO.findById(sessionId);

		request.setAttribute("ArtistInfo", ArtistInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mypage_Artist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");




	}

}
