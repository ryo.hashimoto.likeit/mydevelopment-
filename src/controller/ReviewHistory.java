package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ReviewDAO;
import model.CommonUser;
import model.ReviewBeans;

/**
 * Servlet implementation class ReviewHistory
 */
@WebServlet("/ReviewHistory")
public class ReviewHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8");

		HttpSession session = request.getSession();
		CommonUser user = (CommonUser)session.getAttribute("SessionUser");

		if (user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		int sessionId = user.getId();

		ArrayList<ReviewBeans> reviewList = ReviewDAO.getReviewByUser(sessionId);

		request.setAttribute("reviewList", reviewList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewHistory.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
