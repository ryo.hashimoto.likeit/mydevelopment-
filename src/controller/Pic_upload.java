package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.ArtistDataDAO;
import model.ArtistUser;

/**
 * Servlet implementation class Pic_upload
 */
@WebServlet("/Pic_upload")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-ECR12M6\\Documents\\mydevelopment-\\WebContent\\uploaded/")
public class Pic_upload extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pic_upload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		ArtistUser user = (ArtistUser)session.getAttribute("SessionUser");

		int id = user.getId();
		String fileName = user.getLoginId() + ".jpg";

        Part part = request.getPart("file");

        part.write(fileName);
        //アップロードした画像へのパス
        String picPath = "uploaded/" + fileName;

        ArtistDataDAO.RegistPic(picPath,id);

        response.sendRedirect("MyPage_Artist");
	}

    }


