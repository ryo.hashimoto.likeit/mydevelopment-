package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import dao.GenreDAO;
import model.ArtistUser;
import model.Genres;

/**
 * Servlet implementation class Update_ArtistInfo
 */
@WebServlet("/Update_ArtistInfo")


public class Update_ArtistInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Update_ArtistInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		 HttpSession session = request.getSession();
		 ArtistUser user = (ArtistUser)session.getAttribute("SessionUser");

		if (user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		 int sessionId = user.getId();

		 ArtistUser ArtistInfo = ArtistDataDAO.findById(sessionId);



		 request.setAttribute("ArtistInfo", ArtistInfo);


		 ArrayList<Genres> genre_master = GenreDAO.getGenre();
		 request.setAttribute("genre_master", genre_master);

		 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update_ArtistInfo.jsp");
		 dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String[] genre = request.getParameterValues("genre");
		String[] member = request.getParameterValues("member");
		String region = request.getParameter("region");

		String introduce_nomal = request.getParameter("introduce");
		String[] introduce = introduce_nomal.split("\n");



        ArtistDataDAO.updateArtistInfo(name,startDate,genre,member,region,introduce_nomal,id);


		response.sendRedirect("MyPage_Artist");

	}



}
