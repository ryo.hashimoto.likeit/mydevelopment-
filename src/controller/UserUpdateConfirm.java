package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDataDAO;
import model.CommonUser;

/**
 * Servlet implementation class UserUpdateConfirm
 */
@WebServlet("/UserUpdateConfirm")
public class UserUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		HttpSession session = request.getSession();
		CommonUser user = (CommonUser)session.getAttribute("SessionUser");

		if (user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdateConfirm.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		int id = Integer.parseInt(request.getParameter("id"));

		UserDataDAO dao = new UserDataDAO();

		int num = dao.Update(name, birthDate, id);

		if(num == 1) {
			response.sendRedirect("Index");
			//更新成功
		}else {
			request.setAttribute("errorMessage", "更新失敗。。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("UserUpdateConfirm.jsp");
			dispatcher.forward(request, response);
		}

	}

}
