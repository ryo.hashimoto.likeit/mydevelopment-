package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import dao.FavoriteDAO;
import dao.FollowDAO;
import dao.GenreDAO;
import dao.ReviewDAO;
import model.ArtistUser;
import model.CommonUser;
import model.Genres;
import model.ReviewBeans;

/**
 * Servlet implementation class ArtistPage
 */
@WebServlet("/ArtistPage")
public class ArtistPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArtistPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		int artistId = Integer.parseInt(request.getParameter("id"));

		HttpSession session = request.getSession();
		HttpSession sessionProperty = request.getSession();

		int property = 0;
		if(sessionProperty.getAttribute("property") != null) {
			property =(int) sessionProperty.getAttribute("property");
		}
		//property→ユーザがゲストの場合は０,一般の場合１,アーティストの場合２

		int sessionId = 0;

		if(property == 1) {
			CommonUser common_user = (CommonUser)session.getAttribute("SessionUser");
			sessionId = common_user.getId();

			int favoriteFlag = FavoriteDAO.favoriteFindById(sessionId, artistId);
			request.setAttribute("favoriteFlag", favoriteFlag);
		}else if(property == 2) {
			ArtistUser artist_user = (ArtistUser)session.getAttribute("SessionUser");
			sessionId =  artist_user.getId();

			int favoriteFlag = FollowDAO.followFindById(sessionId, artistId);
			request.setAttribute("favoriteFlag", favoriteFlag);
		}



		ArtistUser artist = ArtistDataDAO.findById(artistId);
		request.setAttribute("artist", artist);

		ArrayList<ReviewBeans> reviewList = ReviewDAO.getReviewByArtist(artistId);
		request.setAttribute("reviewList", reviewList);

		ReviewBeans reviewAvg = ReviewDAO.getAvgRateByArtist(artistId);
		request.setAttribute("reviewAvg", reviewAvg);

		ArrayList<Genres> genre_master = GenreDAO.getGenre();
		request.setAttribute("genre_master", genre_master);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ArtistPage.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		//お気に入り、フォロー追加したときここにIDが送られる
		//新たにfavorite、followコントローラー作るべき？


		String property = request.getParameter("property");

		if(property.equals("common")) {
			//一般ユーザ
			HttpSession session = request.getSession();
			CommonUser user = (CommonUser)session.getAttribute("SessionUser");
			int sessionId = user.getId();

			int artistId = Integer.parseInt(request.getParameter("artistId"));
			int favoriteFlag = Integer.parseInt(request.getParameter("favoriteFlag"));

			if(favoriteFlag == 1) {
				//お気に入り済みの場合（お気に入り削除）
				int result = FavoriteDAO.favoriteDelete(sessionId, artistId);

				if(result >= 1) {
					//成功
					response.sendRedirect("Favorite");
				}else {
					response.sendRedirect("Index");
				}

			}else if(favoriteFlag == 2) {
				//お気に入りしていない場合（お気に入り追加）
				int result = FavoriteDAO.favoriteRegist(sessionId,artistId);

				if(result == 1) {
					//成功
					response.sendRedirect("Favorite");
				}else {
					response.sendRedirect("Index");
				}

			}

		}else if(property.equals("artist")) {
			//アーティストユーザ
			HttpSession session = request.getSession();
			ArtistUser user = (ArtistUser)session.getAttribute("SessionUser");
			int sessionId = user.getId();

			int artistId = Integer.parseInt(request.getParameter("artistId"));
			int favoriteFlag = Integer.parseInt(request.getParameter("favoriteFlag"));

			if(favoriteFlag == 1) {
				//お気に入り済みの場合（お気に入り削除）
				int result = FollowDAO.followDelete(sessionId, artistId);

				if(result >= 1) {
					//成功
					response.sendRedirect("Follow_Artist");
				}else {
					response.sendRedirect("Index");
				}

			}else if(favoriteFlag == 2) {
				//お気に入りしていない場合（お気に入り追加）
				int result = FollowDAO.followRegist(sessionId,artistId);

				if(result == 1) {
					//成功
					response.sendRedirect("Follow_Artist");
				}else {
					response.sendRedirect("Index");
				}


		}




	 }
	}

}
