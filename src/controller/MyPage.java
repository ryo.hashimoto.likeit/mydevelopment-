package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.FavoriteDAO;
import dao.ReviewDAO;
import dao.UserDataDAO;
import model.ArtistUser;
import model.CommonUser;
import model.ReviewBeans;

/**
 * Servlet implementation class MyPage
 */
@WebServlet("/MyPage")
public class MyPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html; charset=UTF-8");

		HttpSession session = request.getSession();
		CommonUser user = (CommonUser)session.getAttribute("SessionUser");

		if (user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		int sessionId = user.getId();

		ArrayList<ReviewBeans> reviewList = ReviewDAO.getReviewByUserNew(sessionId);

		request.setAttribute("reviewList", reviewList);

		ArrayList<ArtistUser> artistList = FavoriteDAO.findFavArtistNew(sessionId);

		request.setAttribute("ArtistList", artistList);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/MyPage.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDataDAO dao = new UserDataDAO();

		CommonUser user = dao.Login(loginId, password);
		//一般ユーザビーンズにパスワード以外のユーザ情報をすべて入れてる

		//ここのdoPOSTは使わないっぽい

	}

}
