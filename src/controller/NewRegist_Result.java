package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDataDAO;
import model.Other;

/**
 * Servlet implementation class NewRegist_Result
 */
@WebServlet("/NewRegist_Result")
public class NewRegist_Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewRegist_Result() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String sex = request.getParameter("sex");
		String birthDate = request.getParameter("birthDate");
		String name = request.getParameter("userName");
		String loginId = request.getParameter("loginId");
		String password = Other.encrypt(request.getParameter("password"));


		UserDataDAO dao = new UserDataDAO();

		int result = dao.CommonUserRegist(name,sex,loginId,password,birthDate);

		if(result == 1) {
			response.sendRedirect("Index");
		}else {
			response.sendRedirect("NewRegist_Common");

		}


	}

}
