package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import model.ArtistUser;
import model.CommonUser;
import model.ReviewBeans;

/**
 * Servlet implementation class Review
 */
@WebServlet("/Review")
public class Review extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Review() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		HttpSession session = request.getSession();
		CommonUser user = (CommonUser)session.getAttribute("SessionUser");

		if (user == null) {
			response.sendRedirect("Index");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移
		//現在はアーティストユーザはレビューできない

		int id = Integer.parseInt(request.getParameter("id"));

		ArtistUser artist = ArtistDataDAO.findById(id);

		request.setAttribute("artist", artist);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Review.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//評価が「評価」のままだったらヌルポになる
		request.setCharacterEncoding("UTF-8");

		int performance = Integer.parseInt(request.getParameter("performance"));
		int technique = Integer.parseInt(request.getParameter("technique"));
		int character = Integer.parseInt(request.getParameter("character"));
		int song = Integer.parseInt(request.getParameter("song"));
		int id = Integer.parseInt(request.getParameter("id"));

		String detail = request.getParameter("detail");

		ReviewBeans review = new ReviewBeans(performance,technique,character,song,detail);

		ArtistUser artist = ArtistDataDAO.findById(id);

		request.setAttribute("review", review);
		request.setAttribute("artist", artist);

		HttpSession session = request.getSession();
		CommonUser user = (CommonUser)session.getAttribute("SessionUser");

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewSubmitConfirm.jsp");
		dispatcher.forward(request, response);
	}

}
