package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ArtistDataDAO;
import model.ArtistUser;

/**
 * Servlet implementation class NewRegist_Artist
 */
@WebServlet("/NewRegist_Artist")
public class NewRegist_Artist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewRegist_Artist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Artist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");



		ArtistUser artist = new ArtistUser();
		artist.setName(name);
		artist.setLoginId(loginId);
		artist.setPassword(password);


		request.setAttribute("artist", artist);

		int errorNum = 0;

		if(name.isEmpty() || loginId.isEmpty() || password.isEmpty()  || password2.isEmpty() ) {
			errorNum = 1;
		}else if(!password.equals(password2)) {
			errorNum = 2;
		}else if(ArtistDataDAO.SearchloginId(loginId) == 1) {
			errorNum = 3;
		}



		switch(errorNum) {
		case 0:
			//成功
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewRegistConfirm_Artist.jsp");
			dispatcher.forward(request, response);
			break;

		case 1:
			request.setAttribute("errorMessage", "未入力の項目があります");

			RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Artist.jsp");
			dispatcher2.forward(request, response);
			break;

		case 2:
			request.setAttribute("errorMessage", "パスワードが異なっています");

			RequestDispatcher dispatcher3 = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Artist.jsp");
			dispatcher3.forward(request, response);
			break;

		case 3:
			request.setAttribute("errorMessage", "既にそのログインIDは存在します");

			RequestDispatcher dispatcher4 = request.getRequestDispatcher("/WEB-INF/jsp/NewRegist_Artist.jsp");
			dispatcher4.forward(request, response);
			break;


	}



	}

}
