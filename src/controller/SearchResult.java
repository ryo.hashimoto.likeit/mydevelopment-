package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import dao.GenreDAO;
import model.ArtistUser;
import model.CommonUser;
import model.Genres;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/SearchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static int PAGE_MAX_ITEM_COUNT = 3;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SearchResult.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		int pageNum;
		if(request.getParameter("pageNum") == null) {
			pageNum = 1;
		}else {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}

		String[] genreStr = request.getParameterValues("genre");

		int[] genreId ;
		if(genreStr != null) {
			genreId  = new int[genreStr.length];
			for(int i = 0; i < genreStr.length; i++) {

				genreId[i] = Integer.parseInt(genreStr[i]);
			}
		}else {
			genreId  = null;

		}


		String region = request.getParameter("region");
		String name = request.getParameter("name");

		double count = ArtistDataDAO.SearchCount(name,region,genreId);
		int TotalPage = (int)Math.ceil(count / PAGE_MAX_ITEM_COUNT);

		List<ArtistUser> artistList = new ArrayList<ArtistUser>();

		//●以下のコードはアーティストリストの中で、お気に入り（フォロー）しているかどうか判定する
		HttpSession session = request.getSession();
		HttpSession sessionProperty = request.getSession();

		int property = 0;
		if(sessionProperty.getAttribute("property") != null) {
			property =(int) sessionProperty.getAttribute("property");
		}
		//property→ユーザがゲストの場合は０,一般の場合１,アーティストの場合２
		//↓それをもとにユーザIDを取得（ゲストの場合は０）

		int sessionId = 0;

		if(property == 1) {
			CommonUser common_user = (CommonUser)session.getAttribute("SessionUser");
			sessionId = common_user.getId();
		}else if(property == 2) {
			ArtistUser artist_user = (ArtistUser)session.getAttribute("SessionUser");
			sessionId =  artist_user.getId();
		}

		//検索結果の並び順をチャックボックスによって分岐
		String arrange = "";

		if(request.getParameter("arrange") != null) {
			arrange = request.getParameter("arrange");
		}

		if(arrange.isEmpty()) {
			artistList = ArtistDataDAO.SearchU(name,region,genreId,pageNum,PAGE_MAX_ITEM_COUNT,sessionId);
			request.setAttribute("arrange",0);
		}else if(arrange.equals("rate")){
			artistList = ArtistDataDAO.SearchArrangeRate(name,region,genreId,pageNum,PAGE_MAX_ITEM_COUNT,sessionId);
			request.setAttribute("arrange",1);
		}else if(arrange.equals("normal")){
			artistList = ArtistDataDAO.SearchU(name,region,genreId,pageNum,PAGE_MAX_ITEM_COUNT,sessionId);
			request.setAttribute("arrange",0);
		}
		//arrange 0→通常並び　1→レート順




		//ページングするときのために検索キーをセット
		request.setAttribute("region", region);
		request.setAttribute("name", name);
		request.setAttribute("genreId", genreId);

		//画像下のお気に入りの出力処理をするためにユーザプロパティを送る
		//現在は一般ユーザのみ表示されるようになっている
		request.setAttribute("property", property);


		request.setAttribute("ArtistList", artistList);
		request.setAttribute("count",(int) count);
		request.setAttribute("TotalPage", TotalPage);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("maxItem", PAGE_MAX_ITEM_COUNT);

		ArrayList<Genres> genre_master = GenreDAO.getGenre();
		request.setAttribute("genre_master", genre_master);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SearchResult.jsp");
		dispatcher.forward(request, response);

	}

}
