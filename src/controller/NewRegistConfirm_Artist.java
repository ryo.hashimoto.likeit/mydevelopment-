package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ArtistDataDAO;
import model.Other;

/**
 * Servlet implementation class NewRegistConfirm_Artist
 */
@WebServlet("/NewRegistConfirm_Artist")
public class NewRegistConfirm_Artist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewRegistConfirm_Artist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewRegistConfirm_Artist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = Other.encrypt(request.getParameter("password"));


		int result = ArtistDataDAO.ArtistUserRegist(name,loginId,password);

		if(result == 1) {
			response.sendRedirect("Index");
		}else {
			request.setAttribute("errMessage", "登録失敗");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewRegistConfirm_Artist.jsp");
			dispatcher.forward(request, response);

		}
	}

}
