package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ArtistDataDAO;
import dao.GenreDAO;
import model.ArtistUser;
import model.CommonUser;
import model.Genres;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html; charset=UTF-8");

			HttpSession session = request.getSession();
			HttpSession sessionProperty = request.getSession();

			int property = 0;
			if(sessionProperty.getAttribute("property") != null) {
				property =(int) sessionProperty.getAttribute("property");
			}
			//property→ユーザがゲストの場合は０,一般の場合１,アーティストの場合２
			//↓それをもとにユーザIDを取得（ゲストの場合は０）

			int sessionId = 0;

			if(property == 1) {
				CommonUser common_user = (CommonUser)session.getAttribute("SessionUser");
				sessionId = common_user.getId();
			}else if(property == 2) {
				ArtistUser artist_user = (ArtistUser)session.getAttribute("SessionUser");
				sessionId =  artist_user.getId();
			}

		 ArrayList<ArtistUser> randomArtist = ArtistDataDAO.randomGetArtist(sessionId);

		 request.setAttribute("ArtistList", randomArtist);

		 ArrayList<Genres> genre_master = GenreDAO.getGenre();
		 request.setAttribute("genre_master", genre_master);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Index.jsp");
		dispatcher.forward(request, response);
	}


}
