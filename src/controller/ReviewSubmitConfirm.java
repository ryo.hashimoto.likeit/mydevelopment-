package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ReviewDAO;

/**
 * Servlet implementation class ReviewSubmitConfirm
 */
@WebServlet("/ReviewSubmitConfirm")
public class ReviewSubmitConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewSubmitConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewSubmitConfirm.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String detail = request.getParameter("detail");
		int performance = Integer.parseInt(request.getParameter("performance"));
		int technique = Integer.parseInt(request.getParameter("technique"));
		int character = Integer.parseInt(request.getParameter("character"));
		int song = Integer.parseInt(request.getParameter("song"));
		Double totalRate = Double.parseDouble(request.getParameter("totalRate"));
		int artistId = Integer.parseInt(request.getParameter("artistId"));
		int userId = Integer.parseInt(request.getParameter("userId"));



		int result = ReviewDAO.reviewRegist(userId,artistId,performance,technique,character,song,totalRate,detail);

		if(result == 1) {
			//レビュー成功
			request.setAttribute("id", artistId);

			response.sendRedirect("Index");

		}else {
			//れびゅー失敗

			request.setAttribute("error", "しっぱい");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewSubmitConfirm.jsp");
			dispatcher.forward(request, response);
		}

	}

}
