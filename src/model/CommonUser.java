package model;

import java.io.Serializable;

public class CommonUser implements Serializable{
	private int id;
	private int property;
	private String sex;
	private String birthDate;
	private String name;
	private String loginId;
	private String password;
	private String password2;
	private String create_date;

	public CommonUser() {

	}

	public CommonUser(String sex,String birthDate,String name,String loginId,String password) {
		this.sex = sex;
		this.birthDate = birthDate;
		this.name = name;
		this.loginId = loginId;
		this.password = password;

	}//新規登録時に使うコンストラクタ

	public CommonUser(int id,int property,String sex,String birthDate,String name,String loginId,String create_date) {
		this.id = id;
		this.property = property;
		this.sex = sex;
		this.birthDate = birthDate;
		this.name = name;
		this.loginId = loginId;
		this.create_date = create_date;

	}//ユーザセッション情報を入れるためのコンストラクタ（パスワード以外）



	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProperty() {
		return property;
	}

	public void setProperty(int property) {
		this.property = property;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
}
