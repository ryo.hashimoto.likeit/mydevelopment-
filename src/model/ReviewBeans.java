package model;

import java.io.Serializable;
import java.math.BigDecimal;

import dao.ArtistDataDAO;
import dao.UserDataDAO;

public class ReviewBeans implements Serializable{
	private int id;
	private int performance;
	private int technique;
	private int character;
	private int song;
	private double totalRate ;
	private String detail;
	private int artistId;
	private int userId;
	private String dateTime;

	private String userName;
	private String artistName;

	private double performance_ave;
	private double technique_ave;
	private double character_ave;
	private double song_ave;
	private double totalRate_ave;

	private int count;

	public ReviewBeans(int performance, int technique, int character, int song, String detail) {
		super();
		this.performance = performance;
		this.technique = technique;
		this.character = character;
		this.song = song;
		this.detail = detail;
		totalRate = (double)(performance + technique + character + song)/4;
		BigDecimal bd = new BigDecimal(totalRate);
		BigDecimal totalRate_s = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		this.totalRate = totalRate_s.doubleValue();
	}//レビュー投稿時に使うコンストラクタ




	public ReviewBeans(double performance_ave, double technique_ave, double character_ave, double song_ave, double totalRate_ave) {
		super();
		BigDecimal p_bd = new BigDecimal(performance_ave);
		BigDecimal p_bd2 = p_bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		this.performance_ave = p_bd2.doubleValue();

		BigDecimal t_bd = new BigDecimal(technique_ave);
		BigDecimal t_bd2 = t_bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		this.technique_ave = t_bd2.doubleValue();

		BigDecimal c_bd = new BigDecimal(character_ave);
		BigDecimal c_bd2 = c_bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		this.character_ave = c_bd2.doubleValue();

		BigDecimal s_bd = new BigDecimal(song_ave);
		BigDecimal s_bd2 = s_bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		this.song_ave = s_bd2.doubleValue();


		BigDecimal total_bd = new BigDecimal(totalRate_ave);
		BigDecimal total_bd2 = total_bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		this.totalRate_ave = total_bd2.doubleValue();

	}//アーティストの各レートの平均値を取得するときに使うコンストラクタ




	public ReviewBeans(int performance, int technique, int character, int song, double totalRate, String detail,
			int userId,String dateTime) {
		super();
		this.performance = performance;
		this.technique = technique;
		this.character = character;
		this.song = song;
		this.totalRate = totalRate;
		this.detail = detail;
		this.userId = userId;
		this.dateTime = dateTime;

		CommonUser user = UserDataDAO.findDataById(userId);
		this.userName = user.getName();
	}//アーティストがされたレビューを取得する時に使うコンストラクタ

	public ReviewBeans(int artistId,int performance, int technique, int character, int song, double totalRate, String detail
			,String dateTime) {
		super();
		this.performance = performance;
		this.technique = technique;
		this.character = character;
		this.song = song;
		this.totalRate = totalRate;
		this.detail = detail;
		this.artistId = artistId;
		this.dateTime = dateTime;

		ArtistUser user = ArtistDataDAO.findById(artistId);
		this.artistName = user.getName();
	}//ユーザがしたレビューを取得するときに使うコンストラクタ




	public ReviewBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}




	public int getPerformance() {
		return performance;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	public int getTechnique() {
		return technique;
	}
	public void setTechnique(int technique) {
		this.technique = technique;
	}
	public int getCharacter() {
		return character;
	}
	public void setCharacter(int character) {
		this.character = character;
	}
	public int getSong() {
		return song;
	}
	public void setSong(int song) {
		this.song = song;
	}
	public double getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(double totalRate) {
		this.totalRate = totalRate;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}




	public int getArtistId() {
		return artistId;
	}




	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}




	public int getUserId() {
		return userId;
	}




	public void setUserId(int userId) {
		this.userId = userId;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getArtistName() {
		return artistName;
	}




	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}




	public String getDateTime() {
		return dateTime;
	}




	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}




	public double getPerformance_ave() {
		return performance_ave;
	}




	public void setPerformance_ave(double performance_ave) {
		this.performance_ave = performance_ave;
	}




	public double getTechnique_ave() {
		return technique_ave;
	}




	public void setTechnique_ave(double technique_ave) {
		this.technique_ave = technique_ave;
	}




	public double getCharacter_ave() {
		return character_ave;
	}




	public void setCharacter_ave(double character_ave) {
		this.character_ave = character_ave;
	}




	public double getSong_ave() {
		return song_ave;
	}




	public void setSong_ave(double song_ave) {
		this.song_ave = song_ave;
	}




	public double getTotalRate_ave() {
		return totalRate_ave;
	}




	public void setTotalRate_ave(double totalRate_ave) {
		this.totalRate_ave = totalRate_ave;
	}




	public int getCount() {
		return count;
	}




	public void setCount(int count) {
		this.count = count;
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}

}
