package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import dao.GenreDAO;
import dao.ReviewDAO;

public class ArtistUser implements Serializable{





	private int id;
	private int property;
	private String name;
	private String region;
	private String introduce;
	private String startDate;
	private String pic;
	private String loginId;
	private String password;
	private String createDate;


	private ArrayList<String>  member_list;

	private int UserfavoriteFlag;

	private int favorite_count;
	private int follow_count;

	public ArtistUser() {

	}

	public ArtistUser(int id, int property,String loginId) {
		super();
		this.id = id;
		this.property = property;

		this.loginId = loginId;

	}


	public ArtistUser(String name, String region, String introduce, String startDate, String pic, String loginId,
			String password ,ArrayList<String> member) {
		this.name = name;
		this.region = region;
		this.introduce = introduce;
		this.startDate = startDate;
		this.pic = pic;
		this.loginId = loginId;
		this.password = password;
		this.member_list = member;
	}




	public ArtistUser(int id, int property, String name, String region, String introduce, String startDate, String pic,
			String loginId, String createDate,ArrayList<String> member_list,int favorite_count,int follow_count) {
		super();
		this.id = id;
		this.property = property;
		this.name = name;
		this.region = region;
		this.introduce = introduce;
		this.startDate = startDate;
		this.pic = pic;
		this.loginId = loginId;
		this.createDate = createDate;
		this.member_list = member_list;
		this.favorite_count = favorite_count;
		this.follow_count = follow_count;
	}


	//アーティストテーブルの情報を取得するために使うコンストラクタ


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProperty() {
		return property;
	}

	public void setProperty(int property) {
		this.property = property;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public ArrayList<String> getGenre() {
		return GenreDAO.findGenreByArtistID(this.id);
	}

	public ArrayList<Integer> getGenreId(){
		return GenreDAO.findGenreIdByArtistID(this.id);
	}

	public int getCountReview() {
		return ReviewDAO.getCount(this.id);
	}

	public double getTotalAvg() {
		BigDecimal total_bd = new BigDecimal(ReviewDAO.getTotalAvg(this.id));
		BigDecimal total_bd2 = total_bd.setScale(1, BigDecimal.ROUND_HALF_UP);

		return total_bd2.doubleValue();
	}




	public ArrayList<String> getMember_list() {
		return member_list;
	}


	public void setMember_list(ArrayList<String> member_list) {
		this.member_list = member_list;
	}

	public int getUserfavoriteFlag() {
		return UserfavoriteFlag;
	}

	public void setUserfavoriteFlag(int userfavoriteFlag) {
		UserfavoriteFlag = userfavoriteFlag;
	}

	public int getFavorite_count() {
		return favorite_count;
	}

	public void setFavorite_count(int favorite_count) {
		this.favorite_count = favorite_count;
	}

	public int getFollow_count() {
		return follow_count;
	}

	public void setFollow_count(int follow_count) {
		this.follow_count = follow_count;
	}


}
