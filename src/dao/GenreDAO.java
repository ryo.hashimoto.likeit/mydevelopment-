package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Genres;

public class GenreDAO {

	public static ArrayList<Genres>  getGenre() {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM genre_m";

			Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            ArrayList<Genres> genres = new ArrayList<Genres>();


			while(rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("genre");

				Genres genre = new Genres(id,name);

				genres.add(genre);
			}

			return genres;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//


	public static String GenreNamefindById(int genreId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM genre_m WHERE id = ? ";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, genreId);
			ResultSet rs = pStmt.executeQuery();

			rs.next();
			String genre_name = rs.getString("genre");


			return genre_name;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ジャンル名をジャンルIDから取得

	public static ArrayList<String> findGenreByArtistID(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();




			String sql = "SELECT * FROM genre t "
					+ "LEFT OUTER JOIN "
					+ "genre_m t1 "
					+ "ON "
					+ "t.genre_id = t1.id "
					+ "WHERE "
					+ "artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, artistId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<String>  genre = new ArrayList<String>();

			while(rs.next()) {
				String genre_name = rs.getString("genre");
				genre.add(genre_name);
			}

			pStmt.close();

			return genre;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//アーティストIDでジャンルリストを取得


	public static ArrayList<Integer> findGenreIdByArtistID(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();




			String sql = "SELECT * FROM genre "
					+ "WHERE "
					+ "artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, artistId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<Integer> genreIdList = new ArrayList<Integer>();

			while(rs.next()) {
				int genre_id = rs.getInt("genre_id");

				genreIdList.add(genre_id);
			}




			pStmt.close();

			return genreIdList;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//アーティストIDでジャンルリストを取得





}
