package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.ArtistUser;

public class FollowDAO {

	public static int followRegist(int artistId_a,int artistId_p) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO follow(artist_id_a,artist_id_p) VALUES (?,?)";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, artistId_a);
			pStmt.setInt(2, artistId_p);


			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //お気に入り追加メソッド

	public static ArrayList<ArtistUser> findFollowArtist(int artistId_a) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "select * from "
					+ "follow t "
					+ "LEFT OUTER JOIN "
					+ "artist t1 "
					+ "ON "
					+ "t.artist_id_p = t1.id "
					+ "WHERE "
					+ "t.artist_id_a = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, artistId_a);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ArtistUser> favArtist = new ArrayList<ArtistUser> ();

			while(rs.next()) {
				ArtistUser artist = new ArtistUser();

				int artistId = rs.getInt("artist_id_p");
				String name = rs.getString("artist_name");
				String region = rs.getString("region");
				String pic = rs.getString("profile_pic");

				artist.setId(artistId);
				artist.setName(name);
				artist.setRegion(region);
				artist.setPic(pic);


				favArtist.add(artist);

			}

			pStmt.close();

			return favArtist;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //ユーザがお気に入りしたアーティスト情報取得メソッド

	public static int followFindById(int artistId_a,int artistId_p) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * from follow WHERE artist_id_a = ? and artist_id_p = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, artistId_a);
			pStmt.setInt(2, artistId_p);


			ResultSet rs = pStmt.executeQuery();
			int result = 0;

			if(rs.next()) {
				result = 1;
			}


			return result;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //お気に入りしているか確認するメソッド

	public static int followDelete(int artistId_a,int artistId_p) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "DELETE from follow WHERE artist_id_a = ? and artist_id_p = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, artistId_a);
			pStmt.setInt(2, artistId_p);


			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //削除メソッド

}
