package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.ArtistUser;

public class FavoriteDAO {

	public static int favoriteRegist(int userId,int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO favorite(user_id,artist_id) VALUES (?,?)";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);
			pStmt.setInt(2, artistId);


			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //お気に入り追加メソッド

	public static ArrayList<ArtistUser> findFavArtist(int userId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "select * from favorite t "
					+ "LEFT OUTER JOIN "
					+ "artist t1 "
					+ "ON "
					+ "t.artist_id = t1.id "
					+ "WHERE "
					+ "t.user_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ArtistUser> favArtist = new ArrayList<ArtistUser> ();

			while(rs.next()) {
				ArtistUser artist = new ArtistUser();

				int artistId = rs.getInt("artist_id");
				String name = rs.getString("artist_name");
				String region = rs.getString("region");
				String pic = rs.getString("profile_pic");

				artist.setId(artistId);
				artist.setName(name);
				artist.setRegion(region);
				artist.setPic(pic);



				favArtist.add(artist);

			}

			pStmt.close();

			return favArtist;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //ユーザがお気に入りしたアーティスト情報取得メソッド

	public static ArrayList<ArtistUser> findFavArtistNew(int userId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "select * from favorite t "
					+ "LEFT OUTER JOIN "
					+ "artist t1 "
					+ "ON "
					+ "t.artist_id = t1.id "
					+ "WHERE "
					+ "t.user_id = ? "
					+ "order by t.id desc limit 3";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ArtistUser> favArtist = new ArrayList<ArtistUser> ();

			while(rs.next()) {
				ArtistUser artist = new ArtistUser();

				int artistId = rs.getInt("artist_id");
				String name = rs.getString("artist_name");
				String region = rs.getString("region");
				String pic = rs.getString("profile_pic");

				int favoriteFlag = FavoriteDAO.favoriteFindById(userId,artistId);
				//お気に入り済みの場合１、お気に入りしていない場合は０

				artist.setId(artistId);
				artist.setName(name);
				artist.setRegion(region);
				artist.setPic(pic);
				artist.setUserfavoriteFlag(favoriteFlag);


				favArtist.add(artist);

			}

			pStmt.close();

			return favArtist;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //ユーザがお気に入りしたアーティスト情報取得メソッド


	public static int favoriteFindById(int userId,int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * from favorite WHERE user_id = ? and artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);
			pStmt.setInt(2, artistId);


			ResultSet rs = pStmt.executeQuery();
			int result = 0;

			if(rs.next()) {
				result = 1;
			}


			return result;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //既にお気に入りがあるか確認するためのメソッド

	public static int favoriteDelete(int userId,int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "DELETE from favorite WHERE user_id = ? and artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);
			pStmt.setInt(2, artistId);


			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //お気に入り削除メソッド



	public static ArrayList<Integer> favoriteFindByUserId(int userId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * from favorite WHERE artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);


			ResultSet rs = pStmt.executeQuery();
			ArrayList<Integer> favoriteArtistList = new ArrayList<Integer>();
			while(rs.next()) {
				int artistId = rs.getInt("artist_id");
				favoriteArtistList.add(artistId);
			}

			pStmt.close();

			return favoriteArtistList;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	}
}//ログイン中のユーザがお気に入り（フォロー）しているアーティストのIDを取得
