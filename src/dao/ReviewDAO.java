package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.ReviewBeans;

public class ReviewDAO {

	public static int reviewRegist(int userId,int artistId,int performance,int technique,int character,int song,Double totalRate,String detail) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "INSERT INTO review(user_id, artist_id, performance_rate, technique_rate, character_rate, song_rate, total_rate, review_detail,create_time) "
					+ "VALUES(?, ? ,? ,? ,? ,? ,? ,?, now())";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setInt(2, artistId);
			pStmt.setInt(3, performance);
			pStmt.setInt(4, technique);
			pStmt.setInt(5, character);
			pStmt.setInt(6, song);
			pStmt.setDouble(7, totalRate);
			pStmt.setString(8, detail);

			int rs = pStmt.executeUpdate();




			pStmt.close();

			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//レビュー登録

	public static ArrayList<ReviewBeans> getReviewByArtist(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "SELECT * FROM review WHERE artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, artistId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ReviewBeans> ReviewList = new ArrayList<ReviewBeans>();

			while(rs.next()) {
				int user_id = rs.getInt("user_id");
				int performance_rate = rs.getInt("performance_rate");
				int technique_rate = rs.getInt("technique_rate");
				int character_rate = rs.getInt("character_rate");
				int song_rate = rs.getInt("song_rate");
				Double total_rate = rs.getDouble("total_rate");
				String review_detail = rs.getString("review_detail");
				String dateTime = rs.getString("create_time");

				ReviewBeans review = new ReviewBeans(performance_rate,technique_rate,character_rate,song_rate,total_rate,review_detail,user_id,dateTime);
				ReviewList.add(review);
			}



			pStmt.close();

			return ReviewList;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//アーティストがされたレビュー取得

	public static ReviewBeans getAvgRateByArtist(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "SELECT "
					+ "AVG(performance_rate),"
					+ "AVG(technique_rate),AVG(character_rate),"
					+ "AVG(song_rate),"
					+ "AVG(total_rate) "
					+ "FROM "
					+ "review "
					+ "WHERE "
					+ "artist_id = ? ";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, artistId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			double performance_avg = rs.getDouble("AVG(performance_rate)");
			double technique_avg = rs.getDouble("AVG(technique_rate)");
			double character_avg = rs.getDouble("AVG(character_rate)");
			double song_avg = rs.getDouble("AVG(song_rate)");
			double total_avg = rs.getDouble("AVG(total_rate)");


			pStmt.close();

			return  new ReviewBeans(performance_avg,technique_avg,character_avg,song_avg,total_avg);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//アーティストがされた各レートの平均を取得



	public static ArrayList<ReviewBeans> getReviewByUser(int userId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "SELECT * FROM review t "
					+ "LEFT OUTER JOIN "
					+ "artist t1 "
					+ "ON "
					+ "t.artist_id = t1.id "
					+ "WHERE user_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ReviewBeans> ReviewList = new ArrayList<ReviewBeans>();

			while(rs.next()) {
				int artist_id = rs.getInt("artist_id");
				int performance_rate = rs.getInt("performance_rate");
				int technique_rate = rs.getInt("technique_rate");
				int character_rate = rs.getInt("character_rate");
				int song_rate = rs.getInt("song_rate");
				Double total_rate = rs.getDouble("total_rate");
				String review_detail = rs.getString("review_detail");
				String dateTime = rs.getString("create_time");
				String artistName = rs.getString("artist_name");
				int id = rs.getInt("t.id");

				ReviewBeans review = new ReviewBeans(artist_id,performance_rate,technique_rate,character_rate,song_rate,total_rate,review_detail,dateTime);
				review.setArtistName(artistName);
				review.setId(id);

				ReviewList.add(review);
			}



			pStmt.close();

			return ReviewList;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ユーザがしたレビュー取得

	public static ArrayList<ReviewBeans> getReviewByReviewId(int reviewId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "SELECT * FROM review t "
					+ "LEFT OUTER JOIN "
					+ "artist t1 "
					+ "ON "
					+ "t.artist_id = t1.id "
					+ "WHERE t.id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, reviewId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ReviewBeans> ReviewList = new ArrayList<ReviewBeans>();

			while(rs.next()) {
				int artist_id = rs.getInt("artist_id");
				int performance_rate = rs.getInt("performance_rate");
				int technique_rate = rs.getInt("technique_rate");
				int character_rate = rs.getInt("character_rate");
				int song_rate = rs.getInt("song_rate");
				Double total_rate = rs.getDouble("total_rate");
				String review_detail = rs.getString("review_detail");
				String dateTime = rs.getString("create_time");
				String artistName = rs.getString("artist_name");
				int id = rs.getInt("t.id");

				ReviewBeans review = new ReviewBeans(artist_id,performance_rate,technique_rate,character_rate,song_rate,total_rate,review_detail,dateTime);
				review.setArtistName(artistName);
				review.setId(id);

				ReviewList.add(review);
			}



			pStmt.close();

			return ReviewList;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//レビューIDでレビュー内容取得


	public static ArrayList<ReviewBeans> getReviewByUserNew(int userId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "select * from review where user_id = ? order by id desc limit 3";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();

			ArrayList<ReviewBeans> ReviewList = new ArrayList<ReviewBeans>();

			while(rs.next()) {
				int artist_id = rs.getInt("artist_id");
				int performance_rate = rs.getInt("performance_rate");
				int technique_rate = rs.getInt("technique_rate");
				int character_rate = rs.getInt("character_rate");
				int song_rate = rs.getInt("song_rate");
				Double total_rate = rs.getDouble("total_rate");
				String review_detail = rs.getString("review_detail");
				String dateTime = rs.getString("create_time");

				ReviewBeans review = new ReviewBeans(artist_id,performance_rate,technique_rate,character_rate,song_rate,total_rate,review_detail,dateTime);
				ReviewList.add(review);
			}



			pStmt.close();

			return ReviewList;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ユーザがしたレビュー取得（最新3件）


	public static int getCount(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "select COUNT(*) from review where artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, artistId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			int count = rs.getInt("COUNT(*)");



			pStmt.close();

			return count;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//アーティストがされたレビュー件数

	public static double getTotalAvg(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "select AVG(total_rate) from review where artist_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, artistId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			double count = rs.getDouble("AVG(total_rate)");



			pStmt.close();

			return count;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//アーティストの総合平均レート

	public static int deleteReview(int reviewId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();


			String sql = "DELETE FROM review WHERE id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, reviewId);

			int rs = pStmt.executeUpdate();

			pStmt.close();

			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//レビュー削除


}
