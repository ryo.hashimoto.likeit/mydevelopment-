package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.ArtistUser;

public class ArtistDataDAO {

	public static int ArtistUserRegist(String name,String login_id,String password) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO artist(artist_name, login_id, password, create_date) "
					+ "VALUES(?, ? ,? , now())";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, login_id);
			pStmt.setString(3, password);
			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //新規登録メソッド

	public static int SearchloginId(String loginId) {
		Connection con = null;
		int num = 0;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM artist WHERE login_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();


			if(rs.next()) {
				num = 1;
			}

			return num;


		} catch (SQLException e) {
			e.printStackTrace();
			return num;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ログインIDの重複を避けるためのメソッド


	public static ArtistUser Login(String loginId, String password) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM artist WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, password);

			ResultSet rs = pStmt.executeQuery();


			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("id");
			int user_property = rs.getInt("user_property");
			String loginid  = rs.getString("login_id");

			pStmt.close();


			return new ArtistUser(id, user_property, loginid);



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ログインメソッド





	public static ArtistUser findById(int artistId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT *"
					+ ",(select count(*)  from favorite where artist_id = " + artistId + ") as favorite_count"
					+ ",(select count(*)  from follow WHERE artist_id_p = " + artistId + ")as follow_count"
					+ " FROM artist WHERE id = "+ artistId + " ";

			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(sql);





			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("id");
			int user_property = rs.getInt("user_property");
			String loginid  = rs.getString("login_id");
			String introduce  = rs.getString("introduce");
			String name = rs.getString("artist_name");
			String region = rs.getString("region");
			String profile_pic = rs.getString("profile_pic");
			String create_date = rs.getString("create_date");
			String start_date = rs.getString("start_date");

			int favorite_count = rs.getInt("favorite_count");
			int follow_count = rs.getInt("follow_count");

			stmt.close();

			//以下は上で取得したアーティストIDからメンバーを取得する
			String sql2 = "SELECT * FROM member WHERE artist_id = ?";

			PreparedStatement pStmt2 = con.prepareStatement(sql2);
			pStmt2.setInt(1, id);

			ResultSet rs2 = pStmt2.executeQuery();

			ArrayList<String>  member_list = new ArrayList<String>();

			while(rs2.next()) {

				String member = rs2.getString("member_name");
				member_list.add(member);
			}

			pStmt2.close();


			/*
			ArrayList<String> genre = OtherDAO.findGenreByArtistID(id);
			*/

			return new ArtistUser(id, user_property, name, region ,introduce, start_date, profile_pic, loginid, create_date,member_list,favorite_count,follow_count);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//IDでアーティスト情報取得




	public static void updateArtistInfo(String name,String startDate,String[] genres,String[] members,String region, String introduce,int id) {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();



			String sql = "UPDATE artist "
					+ "SET "
					+ "artist_name = ?,"
					+ "start_date = ?,"
					+ "region = ?,"
					+ "introduce = ? "
					+ "WHERE id = ?" ;


			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, startDate);
			pStmt.setString(3, region);
			pStmt.setString(4, introduce);
			pStmt.setInt(5, id);

			pStmt.executeUpdate();

			pStmt.close();



			String sel_deleteGenre = "DELETE from genre WHERE artist_id = ?";
			PreparedStatement pStmt_deleteGenre = con.prepareStatement(sel_deleteGenre);

			pStmt_deleteGenre.setInt (1, id);
			pStmt_deleteGenre.executeUpdate();

			pStmt_deleteGenre.close();

			String sql_deleteMember = "DELETE from member WHERE artist_id = ?";
			PreparedStatement pStmt_deleteMember = con.prepareStatement(sql_deleteMember);

			pStmt_deleteMember.setInt (1, id);
			pStmt_deleteMember.executeUpdate();

			pStmt_deleteMember.close();


			if(genres != null) {
				for(String genre : genres) {
					String sql2 = "INSERT INTO genre (artist_id,genre_id) VALUES(?, ?)";
					PreparedStatement pStmt2 = con.prepareStatement(sql2);

					int genre_id = Integer.parseInt(genre);

					pStmt2.setInt(1, id);
					pStmt2.setInt(2, genre_id);

					pStmt2.executeUpdate();

					pStmt2.close();
				}
			}

			if(members != null) {
				for(String member : members) {

					if(!member.isEmpty()) {
						String sql3 = "INSERT INTO member (artist_id,member_name) VALUES(?, ?)";
						PreparedStatement pStmt3 = con.prepareStatement(sql3);

						pStmt3.setInt(1, id);
						pStmt3.setString(2, member);

						pStmt3.executeUpdate();

						pStmt3.close();
					}

				}
			}







		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //アーティスト基本情報更新

	public static void RegistPic(String Pic,int id) {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();



			String sql = "UPDATE artist "
					+ "SET "
					+ "profile_pic = ? "
					+ "WHERE id = ?" ;


			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, Pic);
			pStmt.setInt(2, id);


			pStmt.executeUpdate();

			pStmt.close();



		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //アーティスト基本情報更新





	public static ArrayList<ArtistUser> randomGetArtist(int userId) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "select * from artist order by rand() limit 5";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<ArtistUser> artist = new ArrayList<ArtistUser>();

			while(rs.next()) {
				ArtistUser artists = new ArtistUser();
				int id = rs.getInt("id");

				String name = rs.getString("artist_name");
				String region = rs.getString("region");
				String profile_pic = rs.getString("profile_pic");

				int favoriteFlag = FavoriteDAO.favoriteFindById(userId,id);
				//お気に入り済みの場合１、お気に入りしていない場合は０

				artists.setId(id);
				artists.setName(name);
				artists.setRegion(region);
				artists.setPic(profile_pic);
				artists.setUserfavoriteFlag(favoriteFlag);

				artist.add(artists);
			}
			stmt.close();



			return artist;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//Index用にランダムなアーティスト5件を取得




	public static List<ArtistUser> Search(String name,String region,int[] genreId,int pageNum,int pageMaxItemCount) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// SELECT文を準備
			int startItemNum = (pageNum - 1) * pageMaxItemCount;

			String sql = "SELECT * FROM artist WHERE id >= 0 ";


			if(genreId != null) {
				sql += "AND id in (select artist_id from genre where genre_id in (";

				for(int genreIds : genreId) {
					sql += genreIds + " ,";
				}

				sql += "0))";

			}//複数ジャンル検索

			if(!name.isEmpty()) {
				sql += "AND artist_name LIKE '%" + name + "%' ";
			}

			if(!region.isEmpty()) {
				sql += "AND region LIKE '%" + region + "%' ";
			}

			sql += "ORDER BY id ASC LIMIT " + startItemNum + "," + pageMaxItemCount ;


			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<ArtistUser> artistList = new ArrayList<ArtistUser>();

			while(rs.next()) {
				ArtistUser artists = new ArtistUser();
				int id = rs.getInt("id");
				String ArtistName = rs.getString("artist_name");
				String ArtistRegion = rs.getString("region");
				String profile_pic = rs.getString("profile_pic");

				artists.setId(id);
				artists.setName(ArtistName);
				artists.setRegion(ArtistRegion);
				artists.setPic(profile_pic);

				artistList.add(artists);
			}

			return artistList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}//検索メソッド

	public static List<ArtistUser> SearchU(String name,String region,int[] genreId,int pageNum,int pageMaxItemCount,int userId) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// SELECT文を準備
			int startItemNum = (pageNum - 1) * pageMaxItemCount;

			String sql = "SELECT * FROM artist WHERE id >= 0 ";


			if(genreId != null) {
				sql += "AND id in (select artist_id from genre where genre_id in (";

				for(int genreIds : genreId) {
					sql += genreIds + " ,";
				}

				sql += "0))";

			}//複数ジャンル検索

			if(!name.isEmpty()) {
				sql += "AND artist_name LIKE '%" + name + "%' ";
			}

			if(!region.isEmpty()) {
				sql += "AND region LIKE '%" + region + "%' ";
			}

			sql += "ORDER BY id ASC LIMIT " + startItemNum + "," + pageMaxItemCount ;


			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<ArtistUser> artistList = new ArrayList<ArtistUser>();

			while(rs.next()) {
				ArtistUser artists = new ArtistUser();
				int id = rs.getInt("id");
				String ArtistName = rs.getString("artist_name");
				String ArtistRegion = rs.getString("region");
				String profile_pic = rs.getString("profile_pic");

				int favoriteFlag = FavoriteDAO.favoriteFindById(userId,id);
				//お気に入り済みの場合１、お気に入りしていない場合は０

				artists.setId(id);
				artists.setName(ArtistName);
				artists.setRegion(ArtistRegion);
				artists.setPic(profile_pic);
				artists.setUserfavoriteFlag(favoriteFlag);

				artistList.add(artists);
			}

			return artistList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}//検索メソッド



	public static List<ArtistUser> SearchArrangeRate(String name,String region,int[] genreId,int pageNum,int pageMaxItemCount,int userId) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// SELECT文を準備
			int startItemNum = (pageNum - 1) * pageMaxItemCount;

			String sql = "SELECT *,avg(t1.total_rate) AS totalRateAvg "
					+ "FROM "
					+ "artist t "
					+ "left outer join "
					+ "review t1 "
					+ "ON "
					+ "t.id = t1.artist_id "
					+ "WHERE t.id >= 0 ";

			if(genreId != null) {
				sql += "AND t.id in (select artist_id from genre where genre_id in (";

				for(int genreIds : genreId) {
					sql += genreIds + " ,";
				}

				sql += "0))";

			}//複数ジャンル検索

			if(!name.isEmpty()) {
				sql += "AND artist_name LIKE '%" + name + "%' ";
			}

			if(!region.isEmpty()) {
				sql += "AND region LIKE '%" + region + "%' ";
			}

			sql += "group by "
					+ "t.id "
					+ "ORDER BY totalRateAvg DESC LIMIT " + startItemNum + "," + pageMaxItemCount ;


			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<ArtistUser> artistList = new ArrayList<ArtistUser>();

			while(rs.next()) {
				ArtistUser artists = new ArtistUser();
				int id = rs.getInt("t.id");
				String ArtistName = rs.getString("artist_name");
				String ArtistRegion = rs.getString("region");
				String profile_pic = rs.getString("profile_pic");

				int favoriteFlag = FavoriteDAO.favoriteFindById(userId,id);
				//お気に入り済みの場合１、お気に入りしていない場合は０

				artists.setId(id);
				artists.setName(ArtistName);
				artists.setRegion(ArtistRegion);
				artists.setPic(profile_pic);
				artists.setUserfavoriteFlag(favoriteFlag);

				artistList.add(artists);
			}

			return artistList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}//検索メソッド


	public static int SearchCount(String name,String region,int[] genreId) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// SELECT文を準備


			String sql = "SELECT COUNT(*) FROM artist WHERE id >= 0 ";



			if(genreId != null) {
				sql += "AND id in (select artist_id from genre where genre_id in (";

				for(int genreIds : genreId) {
					sql += genreIds + " ,";
				}

				sql += "0))";

			}//複数ジャンル検索

			if(!name.isEmpty()) {
				sql += "AND artist_name LIKE '%" + name + "%' ";
			}

			if(!region.isEmpty()) {
				sql += "AND region LIKE '%" + region + "%' ";
			}


			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int count = rs.getInt("COUNT(*)");

			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//検索件数取得メソッド





}
