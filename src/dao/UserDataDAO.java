package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.CommonUser;


public class UserDataDAO {
	public int CommonUserRegist(String user_name,String sex,String login_id, String login_passwprd,String birth_date) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "INSERT INTO user(user_name, sex, login_id, login_password,birth_date ,create_date) "
					+ "VALUES(?, ? ,? ,? ,?, now())";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, user_name);
			pStmt.setString(2, sex);
			pStmt.setString(3, login_id);
			pStmt.setString(4, login_passwprd);
			pStmt.setString(5, birth_date);
			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	} //新規登録メソッド

	public static CommonUser Login(String loginId, String password) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and login_password = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, password);

			ResultSet rs = pStmt.executeQuery();


			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("id");
			int user_property = rs.getInt("user_property");
			String loginid  = rs.getString("login_id");
			String name = rs.getString("user_name");
			String sex = rs.getString("sex");
			String birth_date = rs.getString("birth_date");
			String create_date = rs.getString("create_date");

			return new CommonUser(id, user_property, sex, birth_date, name, loginid, create_date);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ログインメソッド


	public int Update(String name, String birthDate,int id) {
		Connection con = null;


		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "UPDATE user SET user_name = ?, birth_date = ? WHERE id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setInt(3, id);

			int rs = pStmt.executeUpdate();


			return rs;


		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//更新メソッド


	public static int SearchloginId(String loginId) {
		Connection con = null;
		int num = 0;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();


			if(rs.next()) {
				num = 1;
			}

			return num;


		} catch (SQLException e) {
			e.printStackTrace();
			return num;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ログインIDの重複を避けるためのメソッド


	public static CommonUser findDataById(int userId) {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);

			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();

			int id = rs.getInt("id");
			int user_property = rs.getInt("user_property");
			String loginid  = rs.getString("login_id");
			String name = rs.getString("user_name");
			String sex = rs.getString("sex");
			String birth_date = rs.getString("birth_date");
			String create_date = rs.getString("create_date");


			return new CommonUser(id, user_property, sex, birth_date, name, loginid, create_date);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//ユーザidでユーザの基本情報を取得

}

